import 'package:flutter/material.dart';
import 'package:mobile/icons/bottom_nav_bar_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class BottomNavBarItem extends StatefulWidget {
  final String label;
  final IconData iconData;
  GestureTapCallback? onTap;
  bool isSelected;

  BottomNavBarItem({Key? key,required this.label,required this.iconData, this.onTap,required this.isSelected, required index}) : super(key: key);

  @override
  _BottomNavBarItemState createState() => _BottomNavBarItemState();
}

class _BottomNavBarItemState extends State<BottomNavBarItem> {
  int? index;

  @override
  Widget build(BuildContext context) {
    return  Expanded(
      child: CustomInkWell(
        onTap: this.widget.onTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              widget.iconData,
              color: this.widget.isSelected ?  Theme.of(context).colorScheme.selectedIconColor :  Theme.of(context).colorScheme.defaultIconColor,
            ),
            SizedBox(height: 9,),
            Text(
              widget.label,
              style: TextStyle(
                fontSize: 10,
                color: this.widget.isSelected ?  Theme.of(context).colorScheme.selectedIconColor :  Theme.of(context).colorScheme.defaultIconColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
