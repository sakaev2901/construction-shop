import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/widgets/item.dart';

class ItemsCarousel extends StatelessWidget {
  const ItemsCarousel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: BoxConstraints(
          minHeight: 100,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: [
            Container(
              height: 217,
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: [
                  Item(),
                  SizedBox(
                    width: 12,
                  ),
                  Item(),
                  SizedBox(
                    width: 12,
                  ),
                  Item(),
                ],
              ),
            )
          ],
        ));
  }
}
