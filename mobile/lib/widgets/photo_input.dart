

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class PhotoInput extends StatefulWidget {

  bool isUploaded;
  final String label;
  final String hint;

  PhotoInput({Key? key, this.isUploaded = false, required this.label, required this.hint}) : super(key: key);

  @override
  _PhotoInputState createState() => _PhotoInputState();
}

class _PhotoInputState extends State<PhotoInput> {

  final ImagePicker _picker = ImagePicker();
  XFile? image;

  void pickImage() async {
    XFile? image  = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      this.image = image;
      this.widget.isUploaded = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: () => pickImage(),
      child: Container(
        height: 60,
        padding: EdgeInsets.only(left: 14, right: 6),
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).accentColor),
          borderRadius: BorderRadius.circular(9),
          color: this.widget.isUploaded ? Theme.of(context).accentColor : Colors.white,
        ),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 12,),
                Text(this.widget.label, style: TextStyle(fontSize: 12, color: Theme.of(context).colorScheme.chipTextColor),),
                Spacer(),
                Text(this.widget.hint, style: TextStyle(fontSize: 16),),
                SizedBox(height: 12,),
              ],
            ),
            Spacer(),
            if (this.widget.isUploaded || this.image != null)
            Row(
              children: [
                Container(
                  height: 24,
                  width: 24,
                  padding: EdgeInsets.only(top: 1.7),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: Theme.of(context).colorScheme.primaryTextColor, width: 1)
                  ),
                  child: Icon(CustomIcons.success, size: 12, color: Theme.of(context).colorScheme.selectedIconColor,),
                ),
                SizedBox(width: 17,),
                Container(
                  height: 48,
                  width: 48,
                  child: ClipRRect(borderRadius: BorderRadius.circular(3),child: Image.file(File(image!.path), fit: BoxFit.cover,)),
                )
              ],
            )
            else Container(
              height: 48,
              width: 48,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(3),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Icon(
                  CustomIcons.plus_7_1,
                  color: Theme.of(context).colorScheme.selectedIconColor,
                  size: 24,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
