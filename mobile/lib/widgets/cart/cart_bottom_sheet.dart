import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/item/item_main_widget.dart';
import 'package:mobile/widgets/main_button.dart';

class CartBottomSheet extends StatefulWidget {
  const CartBottomSheet({Key? key}) : super(key: key);

  @override
  _CartBottomSheetState createState() => _CartBottomSheetState();
}

class _CartBottomSheetState extends State<CartBottomSheet> {
  bool isAddedToCart = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: Constants.padding),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      child: ListView(
        shrinkWrap: true,
        children: [
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () => Navigator.pop(context),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 14),
                  child: Icon(
                    CustomIcons.close,
                    size: 16,
                    color: Theme.of(context).colorScheme.selectedIconColor,
                  ),
                ),
              ),
            ],
          ),
          ItemMainWidget(
            imageWidth: MediaQuery.of(context).size.width * 0.6,
          ),
          SizedBox(
            height: 29,
          ),
          MainButton(
            onPressed: () {
              setState(() {
                this.isAddedToCart = true;
              });
            },
            text: this.isAddedToCart
                ? 'Добавлено'
                : 'Добавить в корзину • 4 500 ₽',
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
