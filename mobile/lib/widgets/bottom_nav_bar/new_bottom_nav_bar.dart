import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/screens/category_screen.dart';
import 'package:mobile/screens/home_screen.dart';
import 'package:mobile/widgets/bottom_nav_bar/bottom_nav_bar_button.dart';
import 'package:mobile/screens/cart_screen.dart';
import 'package:mobile/screens/services_screen.dart';
import 'package:mobile/screens/profile_screen.dart';

class NewNavBar extends StatelessWidget {
  int? selectedPage;

  NewNavBar({Key? key, this.selectedPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 86,
      padding: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
      ),
      child: Row(
        children: [
          BottomNavBarButton(
            icon: CustomIcons.home_10_1,
            label: 'Главная',
            screen: HomeScreen(),
            isSelected: this.selectedPage == 0,
          ),
          BottomNavBarButton(
            icon: CustomIcons.category_7_1,
            label: 'Каталог',
            screen: CategoryScreen(),
            isSelected: this.selectedPage == 1,
          ),
            BottomNavBarButton(
            icon: CustomIcons.buy_6_1,
            isBadged: true,
            label: 'Корзина',
            screen: CartScreen(),
            isSelected: this.selectedPage == 2,
          ),
          BottomNavBarButton(
            icon: CustomIcons.plus_7_1,
            label: 'Услуги',
            screen: ServicesScreen(),
            isSelected: this.selectedPage == 3,
          ),
          BottomNavBarButton(
            icon: CustomIcons.profile_7_1,
            label: 'Профиль',
            screen: ProfileScreen(),
            isSelected: this.selectedPage == 4,
          ),
        ],
      ),
    );
  }
}
