import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class BottomNavBarButton extends StatelessWidget {
  final IconData icon;
  final String label;
  bool isSelected;
  final Widget screen;
  final bool isBadged;

  BottomNavBarButton({
    Key? key,
    required this.icon,
    required this.label,
    required this.screen,
    this.isSelected = false,
    this.isBadged = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: CustomInkWell(
        onTap: () => Navigator.pushReplacement(
          context,
          PageRouteBuilder(
            pageBuilder: (context, animation1, animation2) => this.screen,
            transitionDuration: Duration(seconds: 0),
          ),
        ),
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    this.icon,
                    size: 24,
                    color: this.isSelected
                        ? Theme.of(context).colorScheme.selectedIconColor
                        : Theme.of(context).colorScheme.defaultIconColor,
                  ),
                  SizedBox(height: 10),
                  Text(
                    this.label,
                    style: TextStyle(
                      fontSize: 10,
                      color: this.isSelected
                          ? Theme.of(context).colorScheme.selectedIconColor
                          : Theme.of(context).colorScheme.defaultIconColor,
                    ),
                  )
                ],
              ),
            ),
            Visibility(
              visible: this.isBadged,
              child: Positioned(
                  right: 3,
                  top: 3,
                  child: Container(
                    height: 20,
                    width: 20,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text('3', style: TextStyle(fontWeight: FontWeight.w700),),
                    ),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(10)),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
