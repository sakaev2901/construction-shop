import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/item_screen.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class FAQListItem extends StatelessWidget {
  final String text;
  final IconData icon;
  final Widget screen;

  const FAQListItem({Key? key,required this.text,required this.icon, required this.screen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  CustomInkWell(
      onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (_) => this.screen)),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 17),
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Icon(
                icon,
                color: Theme.of(context).colorScheme.selectedIconColor,
                size: 18,
              ),
            ),
            Text(
              text,
              style: TextStyle(
                color: Theme.of(context).colorScheme.selectedIconColor,
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
