import 'package:flutter/material.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

import 'custom_ink_well.dart';

class CartItem extends StatelessWidget {
  bool isDisabled;
  GestureTapCallback? onTapCount;
  GestureTapCallback? onTapDay;

  CartItem({Key? key, this.isDisabled = false, this.onTapCount, this.onTapDay})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 18, top: 18),
      child: Column(
        children: [
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   children: [
          //     Container(
          //       height: 20,
          //       width: 20,
          //       decoration: BoxDecoration(
          //         borderRadius: BorderRadius.circular(10),
          //         color: Colors.grey
          //       ),
          //       child: Icon(
          //         CustomIcons.close,
          //         size: 12,
          //         color: Colors.white,
          //       ),
          //     )
          //   ],
          // ),
          Row(
            children: [
              Expanded(flex: 1, child: Image.asset('assets/img/elec.png')),
              SizedBox(
                width: 16,
              ),
              Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Text(
                            'Торцовочная пила Metabo KGS305M',
                            style: TextStyle(
                              fontSize: 12,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.grey),
                          child: Icon(
                            CustomIcons.close,
                            size: 12,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    Row(
                      children: [
                        Text(
                          '300 ₽',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          '/сутки',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: this.onTapCount,
                            child: Container(
                              padding: EdgeInsets.only(
                                top: 3,
                                bottom: 3,
                                left: 11,
                                right: 11,
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Theme.of(context).accentColor),
                                color: this.isDisabled
                                    ? Theme.of(context).accentColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  Text('Дней • 5'),
                                  Spacer(),
                                  Visibility(
                                    visible: !this.isDisabled,
                                    child: Column(
                                      children: [
                                        Icon(
                                          CustomIcons.arrow_up,
                                          size: 7,
                                          color: Color(0xFFB6B6B6),
                                        ),
                                        SizedBox(
                                          height: 2.5,
                                        ),
                                        Icon(
                                          CustomIcons.arrow_down,
                                          size: 7,
                                          color: Color(0xFFB6B6B6),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 9,
                        ),
                        Expanded(
                          child: GestureDetector(
                            behavior: HitTestBehavior.opaque,
                            onTap: this.onTapCount,
                            child: Container(
                              padding: EdgeInsets.only(
                                top: 3,
                                bottom: 3,
                                left: 11,
                                right: 11,
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Theme.of(context).accentColor),
                                color: this.isDisabled
                                    ? Theme.of(context).accentColor
                                    : Colors.white,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Row(
                                children: [
                                  Text('Штук • 5'),
                                  Spacer(),
                                  Visibility(
                                    visible: !this.isDisabled,
                                    child: Column(
                                      children: [
                                        Icon(
                                          CustomIcons.arrow_up,
                                          size: 7,
                                          color: Color(0xFFB6B6B6),
                                        ),
                                        SizedBox(
                                          height: 2.5,
                                        ),
                                        Icon(
                                          CustomIcons.arrow_down,
                                          size: 7,
                                          color: Color(0xFFB6B6B6),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 0,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
