import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class ZoomButton extends StatelessWidget {

  GestureTapCallback? onTapPlus;
  GestureTapCallback? onTapMinus;

  ZoomButton({Key? key, this.onTapMinus, this.onTapPlus}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75,
      width: 32,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Color(0xFFF8F8F8),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [

          Expanded(child: CustomInkWell(onTap: this.onTapPlus,child: Container(width: double.infinity,child: Icon(FontAwesomeIcons.plus, size: 12,)))),
          Expanded(child: CustomInkWell(onTap: this.onTapMinus,child: Container(width: double.infinity,child: Icon(FontAwesomeIcons.minus, size: 12,)))),

        ],
      ),
    );
  }
}
