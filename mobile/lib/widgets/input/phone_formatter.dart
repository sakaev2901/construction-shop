import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class PhoneFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length > oldValue.text.length) {
      String text = '';
      if (oldValue.text.length == 0) {
        if (newValue.text == '7') {
          text = '+7';
          return TextEditingValue(
              text: text,
              selection:
                  TextSelection.collapsed(offset: newValue.text.length + 1));
        } else {
          text = '+7 ${newValue.text}';
          return TextEditingValue(
              text: text,
              selection:
                  TextSelection.collapsed(offset: newValue.text.length + 3));
        }
      }
      if (oldValue.text == '+') {
        if (newValue.text == '+7') {
          text = newValue.text;
            return TextEditingValue(
            text: text,
            selection:
                TextSelection.collapsed(offset: newValue.text.length));
        }
        text = '+7${newValue.text.substring(1)}';
        return TextEditingValue(
            text: text,
            selection:
                TextSelection.collapsed(offset: newValue.text.length + 1));
      }
    }
    return newValue;
  }
}
