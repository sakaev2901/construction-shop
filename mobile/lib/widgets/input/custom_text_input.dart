import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/input/input_type.dart';
import 'package:mobile/widgets/input/masked_text_input_formatter.dart';
import 'package:mobile/widgets/input/password_text_input.dart';
import 'package:mobile/widgets/input/phone_formatter.dart';

class CustomTextInput extends StatefulWidget {

  final InputType inputType;
  final String label;
  final String hint;
  double bottomMargin;
  String value = '';

  CustomTextInput({Key? key,required this.label, required this.hint, this.bottomMargin = 0, this.inputType = InputType.STRING}) : super(key: key);

  @override
  _CustomTextInputState createState() => _CustomTextInputState();
}

class _CustomTextInputState extends State<CustomTextInput> {
  String value = '';
  String obscureText = '';

  var _textController = TextEditingController();

  List<TextInputFormatter> _buildFormatters() {
    switch(this.widget.inputType) {
      case InputType.PHONE: {
        return [MaskedTextInputFormatter(mask: '+7 XXX XXX XXXX', separator: ' '), PhoneFormatter(),];
      }
      case InputType.CARD_NUMBER: {
        return [MaskedTextInputFormatter(mask: 'xxxx xxxx xxxx xxxx', separator: ' ')];
      }
      case InputType.CVV: {
        return [MaskedTextInputFormatter(mask: 'xxx', separator: '')];
      }
      case InputType.DATE: {
        return [MaskedTextInputFormatter(mask: 'XX/XX', separator: '/')];
      }
      case InputType.PASSPORT: {
        return [MaskedTextInputFormatter(mask: 'xxxx xxxxxx', separator: ' ')];
      }
      default: return [];
    }
  }

  TextInputType _getKeyboardType() {
    switch (this.widget.inputType) {
      case InputType.CARD_NUMBER:
      case InputType.CVV:
      case InputType.DATE:
      case InputType.PASSPORT:
      case InputType.NUMBER: return TextInputType.number;
      case InputType.PHONE: return TextInputType.phone;
      case InputType.MAIL : return TextInputType.emailAddress;
      default: return TextInputType.text;

    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left:  this.widget.inputType == InputType.PASSWORD ? 8 : 14, top: 10, bottom: 10, right: 14,),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9),
            border: Border.all(color: Theme.of(context).accentColor, width: 1),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: this.widget.inputType == InputType.PASSWORD ? 6 : 0),
                child: Text(this.widget.label, style: TextStyle(fontSize: 12, color: this.value != '' ? Theme.of(context).colorScheme.chipTextColor : Theme.of(context).colorScheme.primaryTextColor,),),
              ),
              SizedBox(height: 9,),
              Container(
                child: TextField(
                  controller: _textController,
                  onChanged: (val) {
                    setState(() {
                      // this.obscureText += '•';
                      // _textController.text = this.obscureText;
                      this.value = val;
                    });
                  },
                  obscureText: this.widget.inputType == InputType.PASSWORD ? true : false,
                  obscuringCharacter: '•',
                  enabled: true,
                  // inputFormatters: [MaskedTextInputFormatter(mask: 'xxxx', separator: '')],
                  inputFormatters: [..._buildFormatters()],
                  style: TextStyle(
                    fontSize: 16,
                    letterSpacing: this.widget.inputType == InputType.PASSWORD ? 12 : 0,
                  ),
                  keyboardType: _getKeyboardType(),
                  decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(vertical: 0),
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Theme.of(context).colorScheme.accentTextColor,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: this.widget.bottomMargin,),
      ],
    );
  }
}
