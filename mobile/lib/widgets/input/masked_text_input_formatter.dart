import 'package:flutter/services.dart';

class MaskedTextInputFormatter extends TextInputFormatter {
  final String mask;
  final String separator;

  MaskedTextInputFormatter({required this.mask, required this.separator});

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    print('${oldValue.text} - ${newValue.text}');
    if (newValue.text.length > 0) {
      if (newValue.text.length > oldValue.text.length) {
        if (newValue.text.length > mask.length) return oldValue;
        if (newValue.text.length < mask.length && mask.substring(newValue.text.length - 1, newValue.text.length - 1 + separator.length) == separator) {
          return TextEditingValue(
            text: '${oldValue.text}$separator${newValue.text.substring(newValue.text.length - 1)}',
            selection: TextSelection.collapsed(offset: newValue.selection.end + separator.length),
          );
        }
      }
      if (newValue.text.length < oldValue.text.length) {
        print(oldValue.text.substring(oldValue.text.length - separator.length, oldValue.text.length) );
        if (oldValue.text.substring(oldValue.text.length - separator.length, oldValue.text.length) == separator) {
          return TextEditingValue(
            text: oldValue.text.substring(0, oldValue.text.length - separator.length),
            selection: TextSelection.collapsed(offset: newValue.text.length - separator.length + 1)
          );
        }
      }
    }
    return newValue;
  }
}
