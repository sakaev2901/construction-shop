import 'package:flutter/material.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class LocationItem extends StatelessWidget {
  bool isSelected;
  GestureTapCallback? onTap;
  final String cityName;

  LocationItem({this.isSelected = false, this.onTap, required this.cityName});

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: this.onTap,
      child: Container(
        padding: EdgeInsets.only(
          top: 20,
          bottom: 20,
          right: 8,
        ),
        child: Row(
          children: [
            Icon(
              CustomIcons.location,
              size: 20,
              color: this.isSelected
                  ? Theme.of(context).colorScheme.selectedIconColor
                  : Theme.of(context).colorScheme.secondaryIconColor,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              this.cityName,
              style: TextStyle(
                fontWeight: FontWeight.w700,
                color: this.isSelected
                    ? Theme.of(context).colorScheme.primaryTextColor
                    : Theme.of(context).colorScheme.secondaryTextColor,
              ),
            ),
            Spacer(),
            if (this.isSelected)
            Icon(
              CustomIcons.success,
              color: Theme.of(context).colorScheme.selectedIconColor,
              size: 20,
            )
          ],
        ),
      ),
    );
  }
}
