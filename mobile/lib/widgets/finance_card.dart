import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';

class FinanceCard extends StatelessWidget {
  final String type;
  final String documentTitle;
  final String cost;
  final String date;

  const FinanceCard(
      {Key? key,
        required this.type,
        required this.documentTitle,
        required this.cost,
        required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Constants.borderRadius),
        color: Theme.of(context).accentColor,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('${this.type} • ${this.date}', style: TextStyle(fontSize: 12, color: Theme.of(context).colorScheme.chipTextColor,),),
                SizedBox(height: 8,),
                Text(this.documentTitle, style: TextStyle(fontSize: 16, height: 1.2),),
                SizedBox(height: 14,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 3, bottom: 3, left: 11, right: 11,),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Theme.of(context).colorScheme.primaryTextColor,
                      ),
                      child: Text(this.cost, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white),),
                    ),
                    SizedBox(width: 11,),
                    Expanded(child: Text('Комментарий по оплате', style: TextStyle(fontSize: 12, color: Theme.of(context).colorScheme.chipTextColor,),)),
                  ],
                )
              ],
            ),
          ),
          Expanded(flex: 2, child: SizedBox(),),
          Icon(CustomIcons.document, color: Theme.of(context).colorScheme.selectedIconColor, size: 26,),
        ],
      ),
    );
  }
}
