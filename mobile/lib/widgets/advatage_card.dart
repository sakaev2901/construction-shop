import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile/config/constants.dart';

class AdvantageCard extends StatelessWidget {
  final String imagePath;
  final String text;

  const AdvantageCard({Key? key,required this.imagePath,required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: [
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: 33,
                    decoration: BoxDecoration(
                      borderRadius:
                      BorderRadius.circular(Constants.borderRadius),
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 7,
                  left: 10,
                  child: SvgPicture.asset(imagePath),
                  width: 55,
                )
              ],
            ),
          ),
          SizedBox(height: 12,),
          Container(
            child: Text(
              text,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
