import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';

class SearchChip extends StatelessWidget {
  final String itemName;

  const SearchChip({Key? key,required this.itemName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 5, right: 5, bottom: 3, top: 3,),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Constants.borderRadius),
        border: Border.all(color: Theme.of(context).primaryColor, width: 1,)
      ),
      child: Text(this.itemName),
    );
  }
}
