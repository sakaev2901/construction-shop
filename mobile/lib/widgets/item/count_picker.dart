import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class CountPicker extends StatefulWidget {
  final String label;
  int initCount;

  CountPicker({Key? key, required this.label, required this.initCount})
      : super(key: key);

  @override
  _CountPickerState createState() => _CountPickerState();
}

class _CountPickerState extends State<CountPicker> {
  TextEditingController? _controller;

  @override
  void initState() {
    this._controller = TextEditingController(text: '1');
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 9,
        bottom: 9,
        left: 10,
        right: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Constants.borderRadius),
        color: Colors.white,
        border: Border.all(color: Theme.of(context).accentColor, width: 1),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            this.widget.label,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: Theme.of(context).colorScheme.secondaryTextColor,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 34,
                width: 34,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Theme.of(context).accentColor,
                ),
                child: CustomInkWell(
                  onTap: () {
                    setState(() {
                      if (int.parse(_controller!.text) > 1) {
                        _controller?.text =
                            (int.parse(this._controller!.text) - 1).toString();
                      }
                    });
                  },
                  child: Icon(
                    CustomIcons.minus,
                    color: Theme.of(context).colorScheme.selectedIconColor,
                    size: 2,
                  ),
                ),
              ),
              // Text('${this.widget.initCount}', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20,),),
              Expanded(
                child: TextFormField(
                  controller: this._controller,
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                  cursorColor: Theme.of(context).colorScheme.primaryTextColor,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                  ),
                ),
              ),
              Container(
                height: 34,
                width: 34,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Theme.of(context).primaryColor,
                ),
                child: CustomInkWell(
                  onTap: () {
                    setState(() {
                      print('hello');
                      _controller?.text =
                          (int.parse(this._controller!.text) + 1).toString();
                      this.widget.initCount++;
                    });
                  },
                  child: Icon(
                    CustomIcons.plus,
                    color: Theme.of(context).colorScheme.selectedIconColor,
                    size: 20,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
