import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class CharacteristicWidget extends StatelessWidget {
  final String name;
  final String value;

  const CharacteristicWidget({Key? key,required this.name,required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 11),
      child: Row(
        children: [
          Text(this.name),
          Spacer(),
          Text(this.value, style: TextStyle(fontWeight: FontWeight.bold),),
        ],
      ),
    );
  }
}
