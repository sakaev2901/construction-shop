import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class PaymentDeliveryTabs extends StatefulWidget {
  const PaymentDeliveryTabs({Key? key}) : super(key: key);

  @override
  _PaymentDeliveryTabsState createState() => _PaymentDeliveryTabsState();
}

class _PaymentDeliveryTabsState extends State<PaymentDeliveryTabs> {
  int selectedTab = 1;

  Map<String, String> paymentChars = {
    'Наличный расчет': 'payment1.svg',
    'На карты Сбербанк, Тиньков': 'payment2.svg',
    'Безналичный расчет с НДС': 'payment3.svg',
    'Безналичный расчет без НДС': 'payment4.svg',
  };

  Map<String, String> deliveryChars = {
    'Доставка осуществляется в пределах города по стоимости от 150 ₽ в одну сторону': 'delivery1.svg',
    'Доставка за пределы города и межгород осуществляется по договоренности': 'delivery2.svg',
    'Самовывоз со склада по адресу: г.Казань, ул. М.Миля д.61': 'delivery3.svg',
  };

  List<Widget> _buildPaymentChars() {
    List<Widget> paymentCharsWidgets = [];
    paymentChars.forEach((key, value) {
      paymentCharsWidgets.add(
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Expanded(flex: 9,child: Text(key)),
                Spacer(),
                Expanded(flex: 1,child: SvgPicture.asset('assets/svg/$value', width: 32,)),
                SizedBox(width: 10,),
              ],
            ),
          )
      );
      paymentCharsWidgets.add(CustomDivider.zero());
    });
    return paymentCharsWidgets;
  }

  List<Widget> _buildDeliveryChars() {
    List<Widget> deliveryCharsWidgets = [];
    deliveryChars.forEach((key, value) {
      deliveryCharsWidgets.add(
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Expanded(flex: 9,child: Text(key)),
                Spacer(),
                Expanded(flex: 1,child: SvgPicture.asset('assets/svg/$value', width: 32,)),
                SizedBox(width: 10,),
              ],
            ),
          )
      );
      deliveryCharsWidgets.add(CustomDivider.zero());
    });
    return deliveryCharsWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomInkWell(
                      onTap: () {
                        setState(() {
                          selectedTab = 1;
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.only(bottom: 20),
                        width: double.infinity,
                        child: Text(
                          'Оплата',
                          style: TextStyle(fontSize: 20, color: selectedTab == 1 ? Theme.of(context).colorScheme.primaryTextColor : Theme.of(context).colorScheme.chipTextColor),
                        ),
                      ),
                    ),
                    selectedTab == 1
                        ? CustomDivider(
                            height: 3,
                            color: Theme.of(context).primaryColor,
                            thickness: 3,
                          )
                        : CustomDivider(height: 3,),
                  ],
                ),
              ),
              Container(
                width: 10,
                height: 3,
                child: CustomDivider(height: 3, color: Color(0xFFF2F2F2),),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomInkWell(
                      onTap: () {
                        setState(() {
                          selectedTab = 2;
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.only(bottom: 20),
                        width: double.infinity,
                        child: Text(
                          'Доставка',
                          style: TextStyle(fontSize: 20, color: selectedTab == 2 ? Theme.of(context).colorScheme.primaryTextColor : Theme.of(context).colorScheme.chipTextColor),
                        ),
                      ),
                    ),
                    selectedTab == 2
                        ? CustomDivider(
                            height: 3,
                            color: Theme.of(context).primaryColor,
                            thickness: 3,
                          )
                        : CustomDivider(height: 3, color: Color(0xFFF2F2F2),),

                  ],
                ),
              ),
            ],
          ),
          if (selectedTab == 1) ..._buildPaymentChars() else  ..._buildDeliveryChars(),
        ],
      ),
    );
  }
}
