import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class InfoCarousel extends StatefulWidget {
  InfoCarousel({Key? key}) : super(key: key);

  @override
  _InfoCarouselState createState() => _InfoCarouselState();
}

class _InfoCarouselState extends State<InfoCarousel> {
  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    final List<Widget> cards = [
      Container(
        height: 140,
        padding: EdgeInsets.symmetric(horizontal: Constants.padding),
        decoration: BoxDecoration(),
        child: Container(
          height: 140,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(
              Constants.borderRadius,
            ),
            child: Image.asset(
              'assets/img/info.png',
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      Container(
        height: 140,
        padding: EdgeInsets.symmetric(horizontal: Constants.padding),
        decoration: BoxDecoration(),
        child: Container(
          height: 140,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(
              Constants.borderRadius,
            ),
            child: Image.asset(
              'assets/img/info.png',
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      Container(
        height: 140,
        padding: EdgeInsets.symmetric(horizontal: Constants.padding),
        decoration: BoxDecoration(),
        child: Container(
          height: 140,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(
              Constants.borderRadius,
            ),
            child: Image.asset(
              'assets/img/info.png',
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    ];

    return Container(
      child: Column(
        children: [
          Container(
            height: 140,
            child: PageView.builder(
              itemBuilder: (context, index) {
                return cards[index % cards.length];
              },
              onPageChanged: (index) {
                setState(() {
                  _currentPage = index;
                });
              },
            ),
          ),
          SizedBox(
            height: 11,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 7,
                width: 7,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: 0 == _currentPage % cards.length
                      ? Theme.of(context).colorScheme.selectedIconColor
                      : Theme.of(context).colorScheme.defaultIconColor,
                ),
              ),
              SizedBox(
                width: 9,
              ),
              Container(
                height: 7,
                width: 7,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: 1 == _currentPage % cards.length
                      ? Theme.of(context).colorScheme.selectedIconColor
                      : Theme.of(context).colorScheme.defaultIconColor,
                ),
              ),
              SizedBox(
                width: 9,
              ),
              Container(
                height: 7,
                width: 7,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: 2 == _currentPage % cards.length
                      ? Theme.of(context).colorScheme.selectedIconColor
                      : Theme.of(context).colorScheme.defaultIconColor,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
