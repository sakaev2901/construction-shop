import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/input/custom_text_input.dart';
import 'package:mobile/widgets/input/input_type.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/widgets/user_data/data_batch_title.dart';

class CompanyTab extends StatelessWidget {
  const CompanyTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 33,),
        DataBatchTitle(title: 'Данные компании'),
        SizedBox(height: 17,),
        CustomTextInput(label: 'Название организации', hint: 'Название организации', bottomMargin: 20,),
        CustomTextInput(label: 'Руководитель', hint: 'Руководитель', bottomMargin: 20,),
        CustomTextInput(label: 'Номер телефона', hint: '+7 XXX XXX XXXX', bottomMargin: 20, inputType: InputType.PHONE,),
        CustomTextInput(label: 'Дата регистрации', hint: 'DD месяц YYYY',),
        SizedBox(height: 39,),
        DataBatchTitle(title: 'Реквизиты компании'),
        SizedBox(height: 17,),
        CustomTextInput(label: 'Юридический адрес', hint: 'Адрес', bottomMargin: 20,),
        CustomTextInput(label: 'ИНН / КПП', hint: 'ИНН / КПП', bottomMargin: 20,),
        CustomTextInput(label: 'Код отрасли по ОКВЭДдрес', hint: 'Код отрасли по ОКВЭД', bottomMargin: 20,),
        CustomTextInput(label: 'ОГРН', hint: 'ОГРН', bottomMargin: 20,),
        CustomTextInput(label: 'Наименование банка', hint: 'Наименование банка', bottomMargin: 20,),
        CustomTextInput(label: 'Расчетный счет', hint: 'Адрес', bottomMargin: 20,),
        CustomTextInput(label: 'Корреспондентский счет', hint: 'Корреспондентский счет', bottomMargin: 20,),
        CustomTextInput(label: 'БИК', hint: 'БИК', bottomMargin: 20,),
        MainButton(text: 'Сохранить',),
        SizedBox(height: 56,),
      ],
    );

  }
}
