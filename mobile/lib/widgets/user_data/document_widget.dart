import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';

class DocumentWidget extends StatelessWidget {
  final String companyName;
  final String documentTitle;
  final String cost;
  final String date;

  const DocumentWidget(
      {Key? key,
      required this.companyName,
      required this.documentTitle,
      required this.cost,
      required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Constants.borderRadius),
        border: Border.all(
          color: Theme.of(context).accentColor,
          width: 1,
        ),
      ),
      // child: Row(
      //   crossAxisAlignment: CrossAxisAlignment.start,
      //   children: [
      //     Expanded(
      //       flex: 10,
      //       child: Column(
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         children: [
      //           Text(this.companyName, style: TextStyle(fontSize: 12, color: Theme.of(context).colorScheme.chipTextColor,),),
      //           SizedBox(height: 8,),
      //           Text(this.documentTitle, style: TextStyle(fontSize: 16, height: 1.2),),
      //           SizedBox(height: 14,),
      //           Row(
      //             children: [
      //               Container(
      //                 padding: EdgeInsets.only(top: 2, bottom: 2, left: 11, right: 11,),
      //                 decoration: BoxDecoration(
      //                   borderRadius: BorderRadius.circular(5),
      //                   color: Theme.of(context).accentColor,
      //                 ),
      //                 child: Text(this.cost, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),
      //               ),
      //               SizedBox(width: 11,),
      //               Text(this.date, style: TextStyle(fontSize: 12, color: Theme.of(context).colorScheme.chipTextColor,),),
      //             ],
      //           )
      //         ],
      //       ),
      //     ),
      //     Expanded(flex: 2, child: SizedBox(),),
      //     Icon(CustomIcons.document, color: Theme.of(context).colorScheme.selectedIconColor, size: 26,),
      //   ],
      // ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(this.companyName, style: TextStyle(fontSize: 12, color: Theme.of(context).colorScheme.chipTextColor,),),
              Icon(CustomIcons.document, color: Theme.of(context).colorScheme.selectedIconColor, size: 26,),
            ],
          ),
          Row(
            children: [
              Expanded(flex: 4,child: Text(this.documentTitle, style: TextStyle(fontSize: 16, height: 1.2),)),
              Expanded(child: Container()),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.only(top: 2, bottom: 2, left: 11, right: 11,),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Theme.of(context).accentColor,
                ),
                child: Text(this.cost, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),),
              ),
              SizedBox(width: 11,),
              Text(this.date, style: TextStyle(fontSize: 12, color: Theme.of(context).colorScheme.chipTextColor,),),
              Spacer(),
              Icon(CustomIcons.upload, color: Theme.of(context).colorScheme.defaultIconColor, size: 26,),

            ],
          )
        ],
      ),
    );
  }
}
