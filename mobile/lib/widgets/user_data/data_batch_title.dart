import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class DataBatchTitle extends StatelessWidget {
  
  final String title;
  
  DataBatchTitle({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(this.title, style: TextStyle(fontSize: 20, color: Theme.of(context).colorScheme.chipTextColor,),);
  }
}
