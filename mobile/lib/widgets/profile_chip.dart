import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';

class ProfileChip extends StatelessWidget {

  final IconData iconData;
  final String title;
  final String subTitle;
  final Color color;

  ProfileChip({Key? key, required this.iconData, required this.title, required this.subTitle, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, bottom: 10, top: 15, right: 15),
      decoration: BoxDecoration(
        color: this.color,
        borderRadius: BorderRadius.circular(Constants.borderRadius),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(this.iconData, size: 28, color: Color.fromRGBO(28, 31, 39, 0.3),),
          SizedBox(height: 13,),
          Text(this.title, style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),),
          SizedBox(height: 4,),
          Text(this.subTitle),
        ],
      ),
    );
  }
}
