import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';

class RentTermCard extends StatelessWidget {

  final String index;
  final String text;

  const RentTermCard({Key? key, required this.text, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 21),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
              borderRadius: BorderRadius.circular(Constants.borderRadius),
            ),
            child: Align(
              alignment: Alignment.center,
              child: Text(this.index, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16,),),
            ),
          ),
          SizedBox(width: 15,),
          Expanded(child: Text(this.text, style: TextStyle(fontSize: 16),)),
        ],
      ),
    );
  }
}
