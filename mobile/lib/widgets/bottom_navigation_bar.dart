import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/icons/bottom_nav_bar_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/bottom_nav_bar_item.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class BottomNavigation extends StatefulWidget {
  GestureTapCallback? onTap1;
  GestureTapCallback? onTap2;
  GestureTapCallback? onTap3;
  GestureTapCallback? onTap4;
  GestureTapCallback? onTap5;
  int selected;

  BottomNavigation({Key? key, this.onTap1, this.onTap2, this.onTap3, this.onTap4, this.onTap5, required this.selected})
      : super(key: key);

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 86,
      padding: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
      ),
      child: Row(
        children: [
          BottomNavBarItem(
              isSelected: widget.selected == 0,
              onTap: widget.onTap1,
              index: 0,
              label: 'Главная',
              iconData: CustomIcons.home_10_1),
          BottomNavBarItem(
              isSelected: widget.selected == 1,
              onTap: widget.onTap2,
              index: 1,
              label: 'Каталог',
              iconData: CustomIcons.category_7_1),
          BottomNavBarItem(
              isSelected: widget.selected == 2,
              label: 'Корзина',
              onTap: this.widget.onTap3,
              index: 2,
              iconData: CustomIcons.buy_6_1),
          BottomNavBarItem(
              isSelected: widget.selected == 3,
              label: 'Услуги',
              onTap: this.widget.onTap4,
              index: 3,
              iconData: CustomIcons.plus_7_1),
          BottomNavBarItem(
              isSelected: widget.selected == 4,
              label: 'Профиль',
              onTap: this.widget.onTap5,
              index: 4,
              iconData: CustomIcons.profile_7_1),
        ],
      ),
    );
  }
}
