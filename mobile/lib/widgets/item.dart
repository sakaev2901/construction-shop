import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/screens/item_screen.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class Item extends StatelessWidget {
  const Item({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: () => Navigator.push(context, CupertinoPageRoute(builder: (_) => ItemScreen())),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 130,
            width: 130,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              border: Border.all(
                color: Theme.of(context).accentColor,
              ),
            ),
            child: Container(
              padding: EdgeInsets.all(10),
              child: Image(
                fit: BoxFit.contain,
                image: AssetImage('assets/img/item.png'),
              ),
            ),
          ),
          SizedBox(height: 7,),
          Container(
            height: 25,
            width: 130,
            child: RichText(
              textAlign: TextAlign.justify,
              overflow: TextOverflow.ellipsis,
              text: TextSpan(
                  text: '10 000 ₽',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                  children: [
                    TextSpan(
                        text: '/сутки',
                        style: TextStyle(
                          fontSize: 10,
                        ))
                  ]),
            ),
          ),
          SizedBox(height: 3,),
          Container(
            width: 130,
            height: 52,
            child: Text(
              'Трансформатор для прогрева бетона КТПТО-80',
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                height: 1.4,
                fontSize: 12,
                color: Theme.of(context).hintColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
