import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/screens/category_screen.dart';
import 'package:mobile/widgets/catalog_carousel_item.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class CatalogCarousel extends StatefulWidget {
  GestureTapCallback? onTap;
  CatalogCarousel({Key? key, this.onTap}) : super(key: key);

  @override
  _CatalogCarouselState createState() => _CatalogCarouselState();
}

class _CatalogCarouselState extends State<CatalogCarousel> {
  int _pageDot = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 200,
          child: NotificationListener<ScrollUpdateNotification>(
            child: ListView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.symmetric(horizontal: Constants.padding - 5),
              children: List.generate(
                10,
                (index) => Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  child: Column(
                    children: [
                      index == 0
                          ? GestureDetector(
                              onTap: () => Navigator.of(context).push(
                                  CupertinoPageRoute(
                                      builder: (_) => CategoryScreen())),
                              child: CatalogCarouselItem(
                                child: Icon(
                                  CustomIcons.category_7_1,
                                  size: 30,
                                ),
                                isIconButton: true,
                              ))
                          : CatalogCarouselItem(
                              child: Image.asset('assets/img/item.png'),
                            ),
                      SizedBox(
                        height: 18,
                      ),
                      CatalogCarouselItem(
                        child: Image.asset('assets/img/item.png'),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            onNotification: (notification) {
              print(notification.metrics.pixels);
              if (notification.metrics.pixels >= 0 &&
                  notification.metrics.pixels < 20) {
                setState(() {
                  _pageDot = 0;
                });
              } else if (notification.metrics.pixels >= 20 &&
                  notification.metrics.pixels < 320) {
                setState(() {
                  _pageDot = 1;
                });
              } else {
                setState(() {
                  _pageDot = 2;
                });
              }
              return true;
            },
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 7,
              width: 7,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _pageDot == 0
                    ? Theme.of(context).colorScheme.selectedIconColor
                    : Theme.of(context).colorScheme.defaultIconColor,
              ),
            ),
            SizedBox(
              width: 9,
            ),
            Container(
              height: 7,
              width: 7,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _pageDot == 1
                    ? Theme.of(context).colorScheme.selectedIconColor
                    : Theme.of(context).colorScheme.defaultIconColor,
              ),
            ),
            SizedBox(
              width: 9,
            ),
            Container(
              height: 7,
              width: 7,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: _pageDot == 2
                    ? Theme.of(context).colorScheme.selectedIconColor
                    : Theme.of(context).colorScheme.defaultIconColor,
              ),
            )
          ],
        )
      ],
    );
  }
}
