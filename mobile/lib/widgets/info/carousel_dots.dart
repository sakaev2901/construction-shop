import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/info/dot.dart';

class CarouselDots extends StatelessWidget {

  final int count;
  final int selectedDotIndex;

  const CarouselDots({Key? key, required this.count, required this.selectedDotIndex}) : super(key: key);
  
  Row _buildDots() {
    List<Widget> dots = [];
    for (int i = 0; i < count; i++) {
      dots.add(
        Dot(color: selectedDotIndex == i ? Color(0xFF27375D) : Color(0xFFD6D6D6))
      );
      if (i != count - 1) {
        dots.add(SizedBox(width: 6,));
      }
    }
    return Row(mainAxisAlignment: MainAxisAlignment.center , children: dots,);
  }

  @override
  Widget build(BuildContext context) {
    return _buildDots();
  }
}
