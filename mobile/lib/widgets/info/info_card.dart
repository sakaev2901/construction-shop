import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class InfoCard extends StatelessWidget {

  final String text;
  final String imageUrl;
  final String buttonText;
  GestureTapCallback? onTap;

  InfoCard({Key? key, required this.text, required this.buttonText, required this.imageUrl, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
            children: [
              Positioned(
                child: Container(
                  width: double.infinity,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        this.imageUrl,
                        height: MediaQuery.of(context).size.height * 0.6,
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.6 / 5,),
                    ],
                  ),
                ),
              ),
             Positioned(
               bottom: 0,
               left: 0,
               right: 0,
               child: Column(
                 children: [
                    Text(
                  this.text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 32,),
                Container(
                  width: MediaQuery.of(context).size.width / 2,
                  child: TextButton(
                    onPressed: onTap,
                    style: TextButton.styleFrom(
                      primary: Theme.of(context).colorScheme.selectedIconColor,
                      minimumSize: Size(double.infinity, 54),
                      backgroundColor: Theme.of(context).primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                    child: Text(
                      this.buttonText,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
                 ],
               ),
             )
            ],
          );
  }
}