import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class Dot extends StatelessWidget {
  final Color color;
  const Dot({Key? key, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 6,
      width: 6,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        color: this.color,
      ),
    );
  }
}
