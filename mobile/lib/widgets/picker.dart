import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Picker extends StatelessWidget {

  const Picker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPicker.builder(
        itemExtent: 32,
        childCount: 100,
        backgroundColor: Colors.white,
        onSelectedItemChanged: (index) {

        },
        itemBuilder: (context, index) {
          return Container(
            child: Align(alignment: Alignment.center,child: Text('${index + 1}')),
          );
        });
  }
}
