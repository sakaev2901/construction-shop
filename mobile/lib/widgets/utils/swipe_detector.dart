import 'package:flutter/material.dart';

class SwipeDetector extends StatelessWidget {

  final Widget child;
  Function onSwipeRight;
  Function onSwipeLeft;

  SwipeDetector({Key? key, required this.child, required this.onSwipeLeft, required this.onSwipeRight}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (details) {
        print(details);
        if (details.delta.dx > 0) {
          onSwipeRight();
        }
        if (details.delta.dx < 0) {
          onSwipeLeft();
        }
      },
      child: this.child,
    );
  }
}
