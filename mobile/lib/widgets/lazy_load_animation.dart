 import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

class LazyLoadAnimation extends StatefulWidget {
  const LazyLoadAnimation({Key? key}) : super(key: key);

  @override
  _LazyLoadAnimationState createState() => _LazyLoadAnimationState();
}

class _LazyLoadAnimationState extends State<LazyLoadAnimation> with SingleTickerProviderStateMixin{
  late RiveAnimationController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = SimpleAnimation('load');
    _controller.isActive = true;
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: 75,
      child: RiveAnimation.asset('assets/svg/load.riv', controllers: [_controller], onInit: (_) {setState(() {});},),

      // Container(
          //   decoration: BoxDecoration(
          //       shape:BoxShape.circle, //making box to circle
          //       color:Colors.deepOrangeAccent //background of container
          //   ),
          //   height:animation?.value, //value from animation controller
          //   width: animation?.value, //value from animation controller
          // ),
    );
  }
}
