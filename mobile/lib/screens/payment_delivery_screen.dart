import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/user_data/data_batch_title.dart';

class PaymentDeliveryScreen extends StatelessWidget {

  Map<String, String> paymentChars = {
    'Наличный расчет': 'payment1.svg',
    'На карты Сбербанк, Тиньков': 'payment2.svg',
    'Безналичный расчет с НДС': 'payment3.svg',
    'Безналичный расчет без НДС': 'payment4.svg',
  };

  Map<String, String> deliveryChars = {
    'Доставка осуществляется в пределах города по стоимости от 150 ₽ в одну сторону': 'delivery1.svg',
    'Доставка за пределы города и межгород осуществляется по договоренности': 'delivery2.svg',
    'Самовывоз со склада по адресу: г.Казань, ул. М.Миля д.61': 'delivery3.svg',
  };


  List<Widget> _buildChars(Map<String, String> chars) {
    List<Widget> widgetChars = [];
    chars.forEach((key, value) {
      widgetChars.add( Container(
        padding: EdgeInsets.symmetric(vertical: 21),
        child: Row(
          children: [
            Expanded(flex: 9,child: Text(key)),
            Spacer(),
            Expanded(flex: 1,child: SvgPicture.asset('assets/svg/$value', width: 32,)),
            SizedBox(width: 10,),
          ],
        ),
      ));
      widgetChars.add(CustomDivider.zero());
    });
    return widgetChars;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Row(
                children: [
                  BaseAppBar(
                    title: 'Оплата и доставка',
                    onTapBack: () => Navigator.pop(context),
                  ),
                ],
              ),
            ),
            Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 30,),
                        DataBatchTitle(title: 'Варианты оплаты'),
                        SizedBox(height: 15,),
                        CustomDivider.zero(),
                        ..._buildChars(paymentChars),
                        SizedBox(height: 30,),
                        DataBatchTitle(title: 'Варианты доставки'),
                        SizedBox(height: 15,),
                        CustomDivider.zero(),
                        ..._buildChars(deliveryChars),
                      ],
                    ),
                  ),
                )),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
