import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/advantages_list.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/widgets/base/item_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/item/characteristics_list.dart';
import 'package:mobile/widgets/item/payment_delivery_tabs.dart';
import 'package:mobile/widgets/item/related_items_list.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/widgets/repair_advantages.dart';

class RepairItemScreen extends StatefulWidget {
  RepairItemScreen({Key? key}) : super(key: key);

  @override
  _RepairItemScreenState createState() => _RepairItemScreenState();
}

class _RepairItemScreenState extends State<RepairItemScreen> {
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: ItemAppBar(
                onTapBack: () => Navigator.pop(context),
                title: "Ремонт инструментов",
                secondButton: BaseIconButton(
                    icon: this.isFavorite
                        ? CustomIcons.star_filled
                        : CustomIcons.star,
                    iconSize: 20,
                    onTap: () {
                      setState(() {
                        this.isFavorite = !this.isFavorite;
                      });
                    }),
              ),
            ),
            SizedBox(
              height: 13,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        child: Image.asset(
                          'assets/img/elec.png',
                          fit: BoxFit.contain,
                        ),
                        width: MediaQuery.of(context).size.width * 0.8,
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      Text(
                        'Ремонт бензопилы Stihl',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 7,
                      ),
                      Text(
                        'от 600 ₽',
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 21,
                      ),
                      RepairAdvantages(),
                      SizedBox(
                        height: 39,
                      ),
                      RelatedItemsList(),
                      SizedBox(
                        height: 36,
                      ),
                      PaymentDeliveryTabs(),
                      SizedBox(
                        height: 10,
                      ),
                      AdvantagesList(),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 17),
                color: Colors.white,
                child: MainButton(
                  onPressed: () => print('Main'),
                  text: 'Заказать ремонт',
                ),
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
