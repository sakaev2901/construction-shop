import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/models/sort_item.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/finance_card.dart';
import 'package:mobile/widgets/sort_bottom_sheet.dart';
import 'package:mobile/widgets/sort_bottom_sheet_item.dart';

import 'filter_screen.dart';

class FinancesScreen extends StatefulWidget {


  FinancesScreen({Key? key}) : super(key: key);

  @override
  _FinancesScreenState createState() => _FinancesScreenState();
}

class _FinancesScreenState extends State<FinancesScreen> {
  int _sortId = 0;

  void _showModal() {
    List<Widget> sheetChildren = [];
    sheetChildren.add(CustomDivider());
    financesSortItems.forEach((element) {
      sheetChildren.add(SortBottomSheetItem(
        name: element.name,
        isActive: _sortId == element.id,
        onTap: () {
          print(element.id);
          Navigator.pop(context);
          setState(() {
            _sortId = element.id;
          });
        },
      ));
      sheetChildren.add(CustomDivider());
    });
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          print('Hello');
          return Container(
            // height: MediaQuery.of(context).size.height * 0.4,
            // constraints: BoxConstraints(
            //   minHeight: 100,
            // ),
            color: Colors.transparent,
            child: SortingBottomSheet(
              children: [...sheetChildren],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: BaseAppBar(
                title: 'Финансы',
                onTapBack: () => Navigator.pop(context),
              ),
            ),
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: CustomDivider.zero(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Row(
                children: [
                  CustomInkWell(
                    onTap: () => Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_) => FilterScreen())),
                    // onTap: () => Navigator.of(context).pushNamed('/filter'),
                    child: Container(
                      height: 50,
                      child: Row(
                        children: [
                          Icon(
                            CustomIcons.filter,
                            size: 16,
                            color:
                            Theme.of(context).colorScheme.selectedIconColor,
                          ),
                          SizedBox(
                            width: 14,
                          ),
                          Text(
                            'Фильтры',
                          )
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  CustomInkWell(
                    onTap: () => _showModal(),
                    child: Container(
                      height: 50,
                      child: Row(
                        children: [
                          Text(
                            financesSortItems[_sortId].name,
                          ),
                          SizedBox(
                            width: 14,
                          ),
                          Icon(
                            CustomIcons.swap,
                            size: 16,
                            color:
                            Theme.of(context).colorScheme.selectedIconColor,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: CustomDivider.zero(),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(height: 28,),
                      FinanceCard(type: 'Продажа', documentTitle: 'Оплата по договору #962478996', cost: '20 000 ₽', date: '15 марта 2021'),
                      SizedBox(height: 20,),
                      FinanceCard(type: 'Продажа', documentTitle: 'Оплата по договору #962478996', cost: '20 000 ₽', date: '15 марта 2021'),
                      SizedBox(height: 20,),
                      FinanceCard(type: 'Продажа', documentTitle: 'Оплата по договору #962478996', cost: '20 000 ₽', date: '15 марта 2021'),
                    ],
                  ),
                ),
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
