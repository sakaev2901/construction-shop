import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/screens/make_order_screen.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/cart_item.dart';
import 'package:mobile/widgets/cart_text_info.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/item/count_picker.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/widgets/picker.dart';

class CartScreen extends StatefulWidget {
  GestureTapCallback? onTap;

  CartScreen({Key? key, this.onTap}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  void _showPicker(String title) {
    // showModalBottomSheet(
    //     context: context,
    //     isScrollControlled: true,
    //     builder: (_) {
    //       return Container(
    //         padding: MediaQuery.of(context).viewInsets,
    //         child: Padding(
    //           padding: EdgeInsets.symmetric(horizontal: Constants.padding),
    //           child: Column(
    //             mainAxisSize: MainAxisSize.min,
    //             children: [
    //               Row(
    //                 children: [
    //                   // Expanded(
    //                   //   child: CountPicker(
    //                   //     label: 'Кол-во дней',
    //                   //     initCount: 5,
    //                   //   ),
    //                   // ),
    //                   // SizedBox(
    //                   //   width: 10,
    //                   // ),
    //                   // Expanded(child: Container()),
    //                   Expanded(
    //                     flex: 1,
    //                     child: CountPicker(
    //                       label: title,
    //                       initCount: 1,
    //                     ),
    //                   ),
    //                   SizedBox(width: 10,),
    //                   Expanded(
    //                     child: MainButton(
    //                       text: 'Применить',
    //                       onPressed: () => Navigator.pop(context),
    //                     ),
    //                   ),
    //                 ],
    //               ),
    //               // SizedBox(height: 10,),
    //
    //               SizedBox(height: 10,),
    //
    //             ],
    //           ),
    //         ),
    //       );
    //     });
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (_) {
          return Container(
            color: Colors.transparent,
            child: Container(height: 200,child: ClipRRect(borderRadius: BorderRadius.only(topRight: Radius.circular(24), topLeft: Radius.circular(24)),child: Picker())),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(
                      left: Constants.padding,
                      right: Constants.padding,
                      top: 33),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Моя корзина',
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 28,
                      ),
                      CustomDivider.zero(),
                      CartItem(
                        onTapCount: () => _showPicker('Кол-в штук'),
                        onTapDay: () => _showPicker('Кол-в дней'),
                      ),
                      CustomDivider.zero(),
                         CartItem(
                        onTapCount: () => _showPicker('Кол-в штук'),
                        onTapDay: () => _showPicker('Кол-в дней'),
                      ),
                      CustomDivider.zero(),
                        CartItem(
                        onTapCount: () => _showPicker('Кол-в штук'),
                        onTapDay: () => _showPicker('Кол-в дней'),
                      ),
                      CustomDivider.zero(),
                        CartItem(
                        onTapCount: () => _showPicker('Кол-в штук'),
                        onTapDay: () => _showPicker('Кол-в дней'),
                      ),
                      CustomDivider.zero(
                        thickness: 2,
                      ),
                      CartTextInfo(name: 'Товары • 2 шт', cost: '5 000 ₽'),
                      CustomDivider.zero(),
                      CartTextInfo(name: 'Доставка', cost: '150 ₽'),
                      CustomDivider.zero(),
                      CartTextInfo(
                        name: 'Моя скидка • 5%',
                        cost: '258 ₽',
                        isDiscount: true,
                      ),
                      CustomDivider.zero(),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: [
                          Text(
                            'Итого',
                            style: TextStyle(
                              fontSize: 19,
                            ),
                          ),
                          Spacer(),
                          Text(
                            '4 892 ₽',
                            style: TextStyle(
                              fontSize: 19,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      MainButton(
                        onPressed: () =>
                            Navigator.of(context).push(CupertinoPageRoute(
                                builder: (_) => MakeOrderScreen(
                                      onTap: this.widget.onTap,
                                    ))),
                        text: 'Оформить заказ',
                      ),
                      SizedBox(
                        height: 17,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            NewNavBar(
              selectedPage: 2,
            ),
          ],
        ),
      ),
    );
  }
}
