import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/advantages_list.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/widgets/base/item_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/item/characteristics_list.dart';
import 'package:mobile/widgets/item/item_main_widget.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/item/payment_delivery_tabs.dart';
import 'package:mobile/widgets/item/related_items_list.dart';
import 'package:mobile/widgets/main_button.dart';

class ItemScreen extends StatefulWidget {
  GestureTapCallback? onTapBack;

  ItemScreen({this.onTapBack});

  @override
  _ItemScreenState createState() => _ItemScreenState();
}

class _ItemScreenState extends State<ItemScreen> {
  bool isFavorite = false;
  bool isAddedToCart = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          SafeArea(
            bottom: false,
            child: Container(
              child: Column(
                children: [
                  SizedBox(
                    height: 17,
                  ),
                  Padding(
                                padding: EdgeInsets.symmetric(horizontal: Constants.padding),

                    child: ItemAppBar(
                      title: "Демонтаж, сверление",
                      secondButton: BaseIconButton(
                          icon: this.isFavorite
                              ? CustomIcons.star_filled
                              : CustomIcons.star,
                          iconSize: 20,
                          onTap: () {
                            setState(() {
                              this.isFavorite = !this.isFavorite;
                            });
                          }),
                    ),
                  ),
                  SizedBox(
                    height: 13,
                  ),
                  Expanded(
                    child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: Constants.padding),

                      child: Stack(
                        children: [
                          SingleChildScrollView(
                            child: Column(
                              children: [
                                ItemMainWidget(
                                  imageWidth: MediaQuery.of(context).size.width * 0.8,
                                ),
                                SizedBox(
                                  height: 39,
                                ),
                                RelatedItemsList(),
                                SizedBox(
                                  height: 36,
                                ),
                                CharacteristicsList(),
                                SizedBox(
                                  height: 36,
                                ),
                                PaymentDeliveryTabs(),
                                SizedBox(
                                  height: 10,
                                ),
                                AdvantagesList(),
                                SizedBox(
                                  height: 100,
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            bottom: 0,
                            left: 0,
                            right: 0,
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: Constants.padding),
                              color: Colors.white,
                              child: MainButton(
                                onPressed: () {
                                  setState(() {
                                    this.isAddedToCart = true;
                                  });
                                },
                                text: this.isAddedToCart ? 'Добавлено' : 'Добавить в корзину • 4 500 ₽',
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  NewNavBar(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
