import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/widgets/advantages_list.dart';
import 'package:mobile/widgets/carousel_title.dart';
import 'package:mobile/widgets/catalog_carousel.dart';
import 'package:mobile/widgets/faq_list.dart';
import 'package:mobile/widgets/info_carousel.dart';
import 'package:mobile/widgets/items_carousel.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/search_widget.dart';

class HomeScreen extends StatelessWidget {
  GestureTapCallback? onTap;
  HomeScreen({
    Key? key,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Search(),
            ),
            SizedBox(
              height: 17,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InfoCarousel(),
                    SizedBox(
                      height: 17,
                    ),
                    CatalogCarousel(
                    ),
                    SizedBox(
                      height: 29,
                    ),
                    CarouselTitle(title: 'Чаще всего арендуют'),
                    SizedBox(
                      height: 17,
                    ),
                    ItemsCarousel(),
                    SizedBox(
                      height: 39,
                    ),
                    CarouselTitle(
                      title: 'Вы уже арендовали',
                    ),
                    SizedBox(
                      height: 17,
                    ),
                    ItemsCarousel(),
                    SizedBox(
                      height: 17,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: Constants.padding),
                      child: AdvantagesList(),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: Constants.padding),
                      child: FAQList(),
                    ),
                    SizedBox(
                      height: 70,
                    )
                  ],
                ),
              ),
            ),
            NewNavBar(selectedPage: 0,),
          ],
        ),
      ),
    );
  }
}
