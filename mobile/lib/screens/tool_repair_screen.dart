import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/models/category.dart';
import 'package:mobile/screens/repair_item_screen.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';

class ToolRepairScreen extends StatelessWidget {
  GestureTapCallback? onTapBack;
  GestureTapCallback? onTapItem;

  ToolRepairScreen({Key? key}) : super(key: key);

  List<Widget> _buildCategories(BuildContext context) {
    List<Widget> categoryWidgets = [];
    categoryWidgets.add(CustomDivider.zero());
    categories.forEach((element) {
      categoryWidgets.add(
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => Navigator.push(
              context, CupertinoPageRoute(builder: (_) => RepairItemScreen())),
          child: Container(
            padding: EdgeInsets.only(
              right: 15,
              top: 12,
              bottom: 12,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 3,
                        child: Text(
                          element.name,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      // Spacer(),
                      Expanded(
                        child: Image.asset(
                          element.imageUrl,
                          height: 50,
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
      categoryWidgets.add(CustomDivider.zero());
    });
    return categoryWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: BaseAppBar(
                title: 'Ремонт инструментов',
                onTapBack: () => Navigator.pop(context),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: SingleChildScrollView(
                  child: Column(
                    children: [..._buildCategories(context)],
                  ),
                ),
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
