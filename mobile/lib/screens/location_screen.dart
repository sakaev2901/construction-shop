import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/location_item.dart';
import 'package:mobile/widgets/main_button.dart';

class LocationScreen extends StatefulWidget {
  const LocationScreen({Key? key}) : super(key: key);

  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  int _selectedCityId = -1;
  Map<int, String> cities = {
    0: 'Магнитогорск',
    1: 'Уфа',
    2: 'Казань',
    3: 'Оренбург',
  };

  List<Widget> _buildLocationList() {
    List<Widget> locationList = [];
    locationList.add(CustomDivider.zero());
    cities.forEach((key, value) {
      locationList.add(LocationItem(
        onTap: () {
          setState(() {
            _selectedCityId = key;
          });
        },
        isSelected: _selectedCityId == key,
        cityName: value,
      ));
      locationList.add(CustomDivider.zero());
    });
    return locationList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: Column(
                  children: [
                    SizedBox(
                      height: 17,
                    ),
                    BaseAppBar(
                      title: 'Выберите ваш регион',
                      onTapBack: () => Navigator.pop(context),
                    ),
                    SizedBox(
                      height: 23,
                    ),
                    Text(
                      'Если ваш регион отсутствует в поиске, выберите ближайший к вам регион',
                      style: TextStyle(
                        color: Theme.of(context).colorScheme.secondaryTextColor,
                        height: 1.4,
                      ),
                    ),
                    SizedBox(
                      height: 21,
                    ),
                    Container(
                      height: 54,
                      padding: EdgeInsets.only(right: 16),
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius:
                            BorderRadius.circular(Constants.borderRadius),
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Icon(
                              CustomIcons.search,
                              size: 18,
                              color: Theme.of(context)
                                  .colorScheme
                                  .selectedIconColor,
                            ),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(bottom: 3),
                              child: TextField(
                                enabled: true,
                                style: TextStyle(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .selectedIconColor,
                                  fontSize: 14,
                                ),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Ваш город',
                                  hintStyle: TextStyle(
                                    color: Theme.of(context)
                                        .colorScheme
                                        .accentTextColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ..._buildLocationList(),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: 16,
              left: Constants.padding,
              right: Constants.padding,
              child: MainButton(
                onPressed: () => Navigator.pop(context),
                text: _selectedCityId == -1 ? 'Выберите регион' : 'Выбрать • ${this.cities[_selectedCityId]}',
              ),
            )
          ],
        ),
      ),
    );
  }
}
