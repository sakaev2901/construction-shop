import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/widgets/base/item_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/review_card.dart';

class AboutScreen extends StatefulWidget {
  const AboutScreen({Key? key}) : super(key: key);

  @override
  _AboutScreenState createState() => _AboutScreenState();
}

class _AboutScreenState extends State<AboutScreen> {
  int _pageDot = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: ItemAppBar(
                onTapBack: () => Navigator.pop(context),
                title: "О компании",
                secondButton: BaseIconButton(
                    icon: CustomIcons.upload, iconSize: 20, onTap: () {}),
              ),
            ),
            SizedBox(
              height: 13,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: Constants.padding,),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 15,
                          ),
                          Image.asset('assets/img/about1.png'),
                          SizedBox(
                            height: 32,
                          ),
                          Text(
                            'Стройка-Аренда - прокат современного и надежного оборудования с доставкой.',
                            style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                height: 1.35),
                          ),
                          SizedBox(
                            height: 32,
                          ),
                          Text(
                            'Стройка Аренда - зарекомендовала себя на рынке как надежная организация, которая способна качественно оказывать услуги с учетом всех основных потребностей Клиентов. За годы работы, мы приобрели бесценный опыт, что помогает нам делать хорошие предложения для наших клиентов. Одним из важнейших критериев качества является удовлетворенность наших заказчиков, мы ценим доверие каждого клиента.',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(0xFF8E929E),
                              height: 1.644,
                            ),
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          Image.asset('assets/img/about2.png'),
                          SizedBox(
                            height: 32,
                          ),
                          Text(
                            'Наша миссия',
                            style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                height: 1.35),
                          ),
                          SizedBox(
                            height: 21,
                          ),
                          Text(
                            'Миссия компании заключается в обеспечении строительной отрасли качественным инструментом и оборудованием для снижения себестоимости строительства. Целью компании является распространение нашего сервиса по всем городам России и обеспечение его доступности. Для этого мы выбрали форму комплексного обслуживания строительных компаний, бригад и частных лиц, что включает в себя услуги по аренде, ремонту и сервисному обслуживанию строительного оборудования и инструмента, а также продаже запасных частей и расходных материалов.',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(0xFF8E929E),
                              height: 1.644,
                            ),
                          ),
                          SizedBox(
                            height: 36,
                          ),
                          Text(
                            'Отзывы клиентов',
                            style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                height: 1.35),
                          ),
                          SizedBox(
                            height: 21,
                          ),
                        ],
                      ),
                    ),
                    // Expanded(
                    //   child: ListView(
                    //     shrinkWrap: true,
                    //     scrollDirection: Axis.horizontal,
                    //     children: [
                    //       ReviewCard(),
                    //       SizedBox(width: 10,),
                    //       ReviewCard(),
                    //
                    //     ],
                    //   ),
                    // ),
                    NotificationListener<ScrollUpdateNotification>(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            SizedBox(
                              width: 17,
                            ),
                            ReviewCard(),
                            SizedBox(
                              width: 10,
                            ),
                            ReviewCard(),
                            SizedBox(
                              width: 10,
                            ),
                            ReviewCard(),
                            SizedBox(
                              width: 17,
                            ),
                          ],
                        ),
                      ),
                      onNotification: (notification) {
                        print(notification.metrics.pixels);
                        if (notification.metrics.pixels >= 0 &&
                            notification.metrics.pixels <
                                MediaQuery.of(context).size.width * .75 -  MediaQuery.of(context).size.width * .25) {
                          setState(() {
                            _pageDot = 0;
                          });
                        } else if (notification.metrics.pixels >=
                            MediaQuery.of(context).size.width * .75 -  MediaQuery.of(context).size.width * .25 &&
                            notification.metrics.pixels <
                                MediaQuery.of(context).size.width * .75 * 2 -
                                    MediaQuery.of(context).size.width * .25) {
                          setState(() {
                            _pageDot = 1;
                          });
                        } else {
                          setState(() {
                            _pageDot = 2;
                          });
                        }
                        return true;
                      },
                    ),
                    SizedBox(
                      height: 17,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _pageDot == 0
                                ? Theme.of(context)
                                    .colorScheme
                                    .selectedIconColor
                                : Color(0xFFD1D4DC),
                          ),
                        ),
                        SizedBox(
                          width: 9,
                        ),
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _pageDot == 1
                                ? Theme.of(context)
                                    .colorScheme
                                    .selectedIconColor
                                : Color(0xFFD1D4DC),
                          ),
                        ),
                        SizedBox(
                          width: 9,
                        ),
                        Container(
                          height: 7,
                          width: 7,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _pageDot == 2
                                ? Theme.of(context)
                                    .colorScheme
                                    .selectedIconColor
                                : Color(0xFFD1D4DC),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 55,
                    ),
                  ],
                ),
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
