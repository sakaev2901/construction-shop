import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/main.dart';
import 'package:mobile/screens/success_order_screen.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/input/custom_text_input.dart';
import 'package:mobile/widgets/input/input_type.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/widgets/payment_type_picker.dart';

class MakeOrderScreen extends StatefulWidget {
  GestureTapCallback? onTap;
  MakeOrderScreen({Key? key, this.onTap}) : super(key: key);

  @override
  _MakeOrderScreenState createState() => _MakeOrderScreenState();
}

class _MakeOrderScreenState extends State<MakeOrderScreen> {

  int _activeIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Constants.padding),

          child: Column(
            children: [
              SizedBox(
                height: 17,
              ),
              BaseAppBar(title: 'Оформление заказа', onTapBack: () => Navigator.pop(context),),
              SizedBox(
                height: 17,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 17,
                      ),
                      Text(
                        'Ваши контакты',
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.chipTextColor,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 17,
                      ),
                      CustomTextInput(
                        label: 'Ваше имя',
                        hint: 'Имя',
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CustomTextInput(
                        label: 'Номер телефона',
                        hint: '+7 XXX XXX XX XX',
                        inputType: InputType.PHONE,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      CustomTextInput(
                        label: 'Электронная почта',
                        hint: 'name@mail.com',
                        inputType: InputType.MAIL,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Адрес доставки',
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.chipTextColor,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 17,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 2,
                            child: CustomTextInput(
                              label: 'Улица и Дом',
                              hint: 'Улица и Дом',
                            ),
                          ),
                          SizedBox(
                            width: 12,
                          ),
                          Expanded(
                            flex: 1,
                            child: CustomTextInput(
                              label: 'Квартира',
                              hint: 'Квартира',
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 39,
                      ),
                      Text(
                        'Способ оплаты',
                        style: TextStyle(
                          color: Theme.of(context).colorScheme.chipTextColor,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 22,
                      ),
                      PaymentTypePicker(onTap: (index) {
                        setState(() {
                          this._activeIndex = index;
                        });
                      }, activeIndex: this._activeIndex,),
                      if (_activeIndex == 0) 
                      Column(
                        children: [
                            SizedBox(
                        height: 23,
                      ),
                      CustomTextInput(
                        label: 'Номер карты',
                        hint: 'XXXX XXXX XXXX XXXX',
                        inputType: InputType.CARD_NUMBER,
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: CustomTextInput(
                              label: 'Срок дейтсвия',
                              hint: 'DD/MM',
                              inputType: InputType.DATE,
                            ),
                          ),
                          SizedBox(width: 16,),
                          Expanded(
                            flex: 2,
                            child: CustomTextInput(
                              label: 'CVV/CVC',
                              hint: 'XXX',
                              inputType: InputType.CVV,
                            ),
                          ),
                          SizedBox(width: 14,),
                          Container(
                              width: 80,
                              child: Text(
                                'Последние 3 цифры на обороте',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Theme.of(context).colorScheme.chipTextColor,
                                ),
                              ))
                        ],
                      ),
                      SizedBox(
                        height: 18,
                      ),
                      CustomTextInput(
                        label: 'Держатель карты',
                        hint: 'IVAN IVANOV',
                      ),
                        ],
                      ),
                      SizedBox(
                        height: 34,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 12.0),
                        child: Text(
                          'Нажимая на кнопку даю согласие на обработку своих персональных данных в соответствии со статьей 9 Федерального закона от 27 июля 2006 г. N 152-ФЗ «О персональных данных»',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.chipTextColor,
                            fontSize: 12,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 34,
                      ),
                      MainButton(
                        onPressed: () => Navigator.pushReplacement(context, CupertinoPageRoute(builder: (_) => SuccessOrderScreen())),
                        text: 'Оплатить заказ',
                      ),
                      SizedBox(height: 22,),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
