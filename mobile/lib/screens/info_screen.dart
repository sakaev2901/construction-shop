import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/parser.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/registration_screen.dart';
import 'package:mobile/widgets/info/carousel_dots.dart';
import 'package:mobile/widgets/info/info_card.dart';

class InfoScreen extends StatefulWidget {
  InfoScreen({Key? key}) : super(key: key);

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  final List<String> cardTexts = [
    'Приложение\nпо аренде оборудования\nпо всей России',
    'Более 30 городов\nпо России где можно\nзабрать оборудование',
    'Более 500 позиций\nпрофессионального\nоборудования в аренду',
    'Регистрируйтесь\n- пройдите проверку\nи пользуйтесь постоянно',
  ];
  PageController? _pageController;
  int _currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    _pageController = PageController(initialPage: _currentPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Container(

          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
          ),
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Container(
                      child: ExpandablePageView.builder(
                          itemCount: cardTexts.length,
                          controller: _pageController,
                          onPageChanged: (i) {
                            setState(() {
                              _currentPage = i;
                            });
                          },
                          itemBuilder: (ctx, i) {
                            String buttonText = '';
                            GestureTapCallback? onTap;
                            if (i == cardTexts.length - 1) {
                              buttonText = 'Регистрация';
                              onTap = () => Navigator.push(
                                  context,
                                  CupertinoPageRoute(
                                      builder: (_) => RegistrationScreen()));
                            } else {
                              buttonText = 'Продолжить';
                              onTap = () {
                                setState(() {
                                  _pageController?.nextPage(
                                      duration: Duration(milliseconds: 400),
                                      curve: Curves.linear);
                                });
                              };
                            }
                            return InfoCard(
                              text: cardTexts[i],
                              buttonText: buttonText,
                              imageUrl: 'assets/img/learn${i + 1}.png',
                              onTap: onTap,
                            );
                          }),
                    ),
                    SizedBox(height: 61,),
                    CarouselDots(count: cardTexts.length, selectedDotIndex: _currentPage),
                  ],
                )),
          ),
        ),
      ),
    );
  }
}
