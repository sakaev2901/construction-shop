import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/check_registration_request_screen.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/input/custom_text_input.dart';
import 'package:mobile/widgets/input/input_type.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/widgets/photo_input.dart';
import 'package:mobile/widgets/user_data/data_batch_title.dart';

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Constants.padding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 17,
              ),
              BaseAppBar(
                title: 'Регистрация',
                onTapBack: () => Navigator.pop(context),
              ),
              Expanded(
                  child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 41,
                    ),
                    DataBatchTitle(title: 'Ваши контакты'),
                    SizedBox(
                      height: 18,
                    ),
                    CustomTextInput(
                      label: 'Ваше имя',
                      hint: 'Имя',
                      bottomMargin: 20,
                    ),
                    CustomTextInput(
                      label: 'Фамилия',
                      hint: 'Фамилия',
                      bottomMargin: 20,
                    ),
                    CustomTextInput(
                      label: 'Номер телефона',
                      hint: '+7 XXX XXX XXXX',
                      bottomMargin: 20,
                      inputType: InputType.PHONE,
                    ),
                    CustomTextInput(
                      label: 'Электронная почта',
                      hint: 'name@mail.com',
                      inputType: InputType.MAIL,
                    ),
                    SizedBox(
                      height: 41,
                    ),
                    DataBatchTitle(title: 'Паспортные данные'),
                    SizedBox(
                      height: 18,
                    ),
                    CustomTextInput(
                      label: 'Серия и номер паспорта',
                      hint: '1234 123456',
                      bottomMargin: 20,
                      inputType: InputType.PASSPORT,
                    ),
                    CustomTextInput(
                      label: 'Кем выдан',
                      hint: 'УФМС...',
                      bottomMargin: 20,
                    ),
                    CustomTextInput(
                      label: 'Код подразделения',
                      hint: '123456',
                      bottomMargin: 20,
                      inputType: InputType.NUMBER,
                    ),
                    CustomTextInput(
                      label: 'Когда выдан',
                      hint: 'DD месяц YYYY',
                    ),
                    SizedBox(
                      height: 37,
                    ),
                    DataBatchTitle(title: 'Скан паспорта'),
                    SizedBox(
                      height: 18,
                    ),
                    PhotoInput(label: 'Первая страница паспорта', hint: 'Загрузить фото',),
                    SizedBox(height: 20,),
                    PhotoInput(label: 'Первая страница паспорта', hint: 'Загрузить фото'),
                    SizedBox(height: 20,),
                    MainButton(
                      text: 'Отправить данные на проверку',
                      onPressed: () => Navigator.of(context).push(
                          CupertinoPageRoute(builder: (_) => SuccessRequestScreen(barTitle: 'Регистрация', mainTitle: 'Заявка принята!\nДанные на проверке.', text: 'Проверка займет до 10 минут.\nПосле прохождения проверки ваш заказ автоматически попадет в работу.\nМы оповестим вас о прохождении проверки.',))),
                    ),
                    SizedBox(
                      height: 21,
                    ),
                    Container(
                      width: double.infinity,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Нажимая на кнопку вы принимаете условия обработки персональных данных и политику конфиденциальности',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.chipTextColor,
                            fontSize: 12,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 61,
                    ),
                  ],
                ),
              )),
            ],
          ),
        ),
      ),
    );
  }
}
