import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/home_screen.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/cart_item.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/main_button.dart';

class SuccessOrderScreen extends StatelessWidget {
  GestureTapCallback? onTap;

  SuccessOrderScreen({Key? key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: 
            Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.only(
                      top: 18,
                      left: Constants.padding,
                      right: Constants.padding,
                    ),
                    child: Column(
                      children: [
                        Container(
                          height: 74,
                          width: 74,
                          padding: EdgeInsets.all(11),
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(24),
                          ),
                          child: SvgPicture.asset('assets/svg/success_square.svg'),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Text(
                          'Отлично ваш заказ оформлен!',
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 27, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 13,
                        ),
                        Text(
                          'Дождитесь звонка оператора для уточнения деталей вашего заказа',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.chipTextColor,
                          ),
                        ),
                        SizedBox(
                          height: 17,
                        ),
                        CustomDivider.zero(),
                        CartItem(isDisabled: true,),
                        CustomDivider.zero(),
                        CartItem(isDisabled: true,),
                        CustomDivider.zero(),
                        CartItem(isDisabled: true,),
                        SizedBox(height: 100,),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(Constants.padding),
                  child: MainButton(
                    onPressed: () => Navigator.pushReplacement(context, CupertinoPageRoute(builder: (_) => HomeScreen())),
                    text:  'Прекрасно',
                    ),
                ),
                NewNavBar(),
              ],
            ),
        ),
    );
  }
}
