import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/screens/service_item_screen.dart';
import 'package:mobile/screens/tool_repair_screen.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/service_grid_item.dart';

class ServicesScreen extends StatelessWidget {
  const ServicesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Expanded(
              // child: SingleChildScrollView(),
              child: SingleChildScrollView(
                padding: EdgeInsets.only(
                    top: 33, right: Constants.padding, left: Constants.padding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Услуги',
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 33,
                    ),
                    GridView.count(
                      primary: false,
                      crossAxisCount: 2,
                      shrinkWrap: true,
                      childAspectRatio: 166 / 180,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      children: [
                        CustomInkWell(
                            onTap: () => Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (_) => ToolRepairScreen())),
                            child: ServiceGridItem(
                                name: 'Ремонт инструментов',
                                imageUrl: 'assets/img/service1.png',
                                startingCost: 700)),
                        CustomInkWell(
                            onTap: () => Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (_) => ServiceItemScreen())),
                            child: ServiceGridItem(
                                name: 'Тепловизионное обследование',
                                imageUrl: 'assets/img/service2.png',
                                startingCost: 1000)),
                        CustomInkWell(
                            onTap: () => Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (_) => ServiceItemScreen())),
                            child: ServiceGridItem(
                                name: 'Измельчение древесных отходов',
                                imageUrl: 'assets/img/service3.png',
                                startingCost: 1750)),
                        CustomInkWell(
                            onTap: () => Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (_) => ServiceItemScreen())),
                            child: ServiceGridItem(
                                name: 'Резка плитки, керамогранита',
                                imageUrl: 'assets/img/service4.png',
                                startingCost: 120)),
                        CustomInkWell(
                            onTap: () => Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (_) => ServiceItemScreen())),
                            child: ServiceGridItem(
                                name: 'Алмазное бурение и сверление',
                                imageUrl: 'assets/img/service5.png',
                                startingCost: 1000)),
                        CustomInkWell(
                            onTap: () => Navigator.push(
                                context,
                                CupertinoPageRoute(
                                    builder: (_) => ServiceItemScreen())),
                            child: ServiceGridItem(
                                name: 'Сварка и стыковка полиэтиленовых труб',
                                imageUrl: 'assets/img/service6.png',
                                startingCost: 1000)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            NewNavBar(selectedPage: 3,),
          ],
        ),
      ),
    );
  }
}
