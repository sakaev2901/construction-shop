class FilterModel {
  final String name;
  final int amount;
  final int id;

  FilterModel({required this.name,required this.amount,required this.id});
}

List<FilterModel> filterModels = [
  FilterModel(name: 'Bosch', amount: 11, id: 0),
  FilterModel(name: 'Dexter', amount: 2, id: 1),
  FilterModel(name: 'Makita', amount: 31, id: 2),
  FilterModel(name: 'Metabo', amount: 5, id: 3),
  FilterModel(name: 'Metabo', amount: 5, id: 4),
];