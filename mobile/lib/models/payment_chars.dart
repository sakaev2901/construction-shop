Map<String, String> paymentChars = {
  'Наличный расчет': 'payment1.svg',
  'На карты Сбербанк, Тиньков': 'payment2.svg',
  'Безналичный расчет с НДС': 'payment3.svg',
  'Безналичный расчет без НДС': 'payment4.svg',
};