class FilterSelectedRange {
  double upperValue;
  double lowerValue;
  final int id;
  final String name;
  final double max;
  final double min;

  FilterSelectedRange(
      {required this.upperValue,
      required this.lowerValue,
      required this.id,
      required this.name,
      required this.max,
      required this.min});
}
