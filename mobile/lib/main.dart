import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:mobile/screens/cart_screen.dart';
import 'package:mobile/screens/category_items_screen.dart';
import 'package:mobile/screens/category_screen.dart';
import 'package:mobile/screens/favorites_screen.dart';
import 'package:mobile/screens/filter_screen.dart';
import 'package:mobile/screens/home_screen.dart';
import 'package:mobile/screens/info_screen.dart';
import 'package:mobile/screens/item_screen.dart';
import 'package:mobile/screens/location_screen.dart';
import 'package:mobile/screens/login_screen.dart';
import 'package:mobile/screens/make_order_screen.dart';
import 'package:mobile/screens/profile_screen.dart';
import 'package:mobile/screens/services_screen.dart';
import 'package:mobile/screens/success_order_screen.dart';
import 'package:mobile/route/navigator_route.dart';
import 'package:mobile/widgets/bottom_navigation_bar.dart';
import 'package:mobile/widgets/search_widget.dart';

void main() {
  // debugPaintSizeEnabled=true;

  runApp(MyApp(index: 0,));
}

class MyApp extends StatefulWidget {
  int index = 0;

  MyApp({required this.index});

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  void onTapCategory() {
    setState(() {
      widget.index = 5;
    });
  }

  @override
  Widget build(BuildContext context) {
    Search search = Search(onTapFavorites: () {
      setState(() {
        widget.index = 7;
      });
    },);
    List<Widget> _pages = [
      HomeScreen(),
      CategoryScreen(
        onTap: onTapCategory,
      ),
      CartScreen(onTap: () {
        setState(() {
          widget.index = 8;
        });
      },),
      ServicesScreen(),
      ProfileScreen(),
      CategoryItemsScreen(
        onTap: () {
          setState(() {
            widget.index = 1;
          });
        },
        onTapItem: () {
          setState(() {
            widget.index = 6;
          });
        },
      ),
      ItemScreen(onTapBack: () {
        setState(() {
          widget.index = 5;
        });
      },),
      FavoritesScreen(onTap: () {
        setState(() {
          widget.index = 0;
        });
      },),
      SuccessOrderScreen(
        onTap: () {
          setState(() {
            widget.index = 0;
          });
        },
      ),
    ];
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Color(0xFFFFDD55),
        accentColor: Color(0xFFF1F1F1),
        hintColor: Color(0xFF6F7481),
        dividerColor: Color(0xFFF2F2F2),
        canvasColor: Colors.transparent,
        // textButtonTheme: TextButtonThemeData(
        //   style: ButtonStyle(
        //     textStyle: MaterialStateProperty.all<TextStyle>(
        //       TextStyle(
        //         color: Color(0xFF1C1F27),
        //       )
        //     ),
        //   ),
        // ),
        textTheme: TextTheme(
          bodyText2: TextStyle(
            color: Color(0xFF1C1F27),
            height: 1,
          ),
        ),
      ),
      // home: Scaffold(
      //   backgroundColor: Colors.white,
      //   // body: SafeArea(child: Navigator(onGenerateRoute: RouteGenerator.generateRoute, initialRoute: '/',)),
      //   body: SafeArea(child: _pages[widget.index]),
      //   // body: SafeArea(child: SuccessOrderScreen()),
      //   bottomNavigationBar: BottomNavigation(
      //     selected: widget.index,
      //     onTap1: () {
      //       setState(() {
      //         widget.index = 0;
      //       });
      //     },
      //     onTap2: () {
      //       setState(() {
      //         widget.index = 1;
      //       });
      //     },
      //     onTap3: () {
      //       setState(() {
      //         widget.index = 2;
      //       });
      //     },
      //     onTap4: () {
      //       setState(() {
      //         widget.index = 3;
      //       });
      //     },
      //     onTap5: () {
      //       setState(() {
      //         widget.index = 4;
      //       });
      //     },
      //   ),
      // ),
      // home: HomeScreen(),
      home: InfoScreen(),
      // routes: {
      //   '/filter': (_) => Scaffold(body: Text('Text'),),
      // },
    );
  }
}
