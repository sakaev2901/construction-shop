class FilterRange {
  final String name;
  final double max;
  final double min;
  final int id;

  FilterRange(this.min, this.max, this.name, this.id);
}

List<FilterRange> filterRanges = [
  FilterRange(1, 100, 'Мощность (Вт)', 0),
  FilterRange(1, 55, 'Сила удара (Дж)', 1),
  FilterRange(1, 78, 'Диаметр сверления в бетоне (мм)', 2),
];