class SortItem {
  final int id;
  final String name;

  SortItem(this.id, this.name);
}


List<SortItem> sortItems = [
  SortItem(0, 'Цена по возрастанию'),
  SortItem(1, 'Цена по убыванию'),
  SortItem(2, 'Рекомендуем'),
];


List<SortItem> financesSortItems = [
  SortItem(0, 'По дате'),
  SortItem(1, 'По цене'),
  // SortItem(2, 'Рекомендуем'),
];

