class Category {
  final String name;
  final String imageUrl;
  bool isSelected = false;
  List<SubCategory> subCategories = [
    SubCategory(name: 'Отбойные молотки', amount: 27),
    SubCategory(name: 'Перфораторы', amount: 7),
    SubCategory(name: 'Штроборезы', amount: 15),
    SubCategory(name: 'Торцовочные пилы', amount: 11),
  ];

  Category({required this.name,required this.imageUrl});

}

List<Category> categories = [
  Category(name: 'Дорожно-строительная техника', imageUrl: 'assets/img/road.png'),
  Category(name: 'Электростанции', imageUrl: 'assets/img/saw.png'),
  Category(name: 'Электроинструменты', imageUrl: 'assets/img/elec.png'),
  Category(name: 'Электроинструменты', imageUrl: 'assets/img/elec.png'),
  Category(name: 'Электроинструменты', imageUrl: 'assets/img/elec.png'),
  Category(name: 'Электроинструменты', imageUrl: 'assets/img/elec.png'),
  Category(name: 'Электроинструменты', imageUrl: 'assets/img/elec.png'),
  Category(name: 'Электроинструменты', imageUrl: 'assets/img/elec.png'),
  Category(name: 'Электроинструменты', imageUrl: 'assets/img/elec.png'),
];

class SubCategory {
  final String name;
  final int amount;

  SubCategory({required this.name,required this.amount});
}

List<SubCategory> subCategories = [
  SubCategory(name: 'Отбойные молотки', amount: 27),
  SubCategory(name: 'Перфораторы', amount: 7),
  SubCategory(name: 'Штроборезы', amount: 15),
  SubCategory(name: 'Торцовочные пилы', amount: 11),
];