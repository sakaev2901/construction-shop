import 'package:flutter/material.dart';
import 'package:mobile/screens/category_items_screen.dart';
import 'package:mobile/screens/category_screen.dart';
import 'package:mobile/screens/filter_screen.dart';
import 'package:mobile/screens/home_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch(settings.name) {
      // case '/':
      //   return MaterialPageRoute(builder: (_) => HomeScreen());
      // case '/category':
      //   return MaterialPageRoute(builder: (_) => CategoryScreen());
      // case '/category-items':
      //   return MaterialPageRoute(builder: (_) => CategoryItemsScreen());
      case '/filter':
        return MaterialPageRoute(builder: (_) => FilterScreen());
      default:
        return errorPage();
    }
  }

  static Route<dynamic> errorPage() {
    return MaterialPageRoute(builder: (_) => Scaffold(body: Text('error'), bottomNavigationBar: null,));
  }
}