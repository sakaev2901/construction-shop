import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/order/order_item.dart';

class OrdersListScreen extends StatefulWidget {
  const OrdersListScreen({Key? key}) : super(key: key);

  @override
  _OrdersListScreenState createState() => _OrdersListScreenState();
}

class _OrdersListScreenState extends State<OrdersListScreen> {

  List<Widget> _tabs = [
    Column(children: [
      OrderItem(cost: 12590, date: '28 октября 2020', orderId: 547865789, orders: ['saw.png', 'elec.png', 'item.png', 'item.png'], rentDayCount: 8),
      SizedBox(height: 20,),
      OrderItem(cost: 6000, date: '28 октября 2020', orderId: 547835789, orders: ['road.png', 'cart_item.png',], rentDayCount: 8),
    ],),
    Column(children: [],),
    Column(children: [],),
  ];
  int _selectedTab = 0;

  Widget _buildTabTitles() {
    Map<int, String> titles = {
      0: 'Аренда',
      1: 'Покупка',
      2: 'Спрос',
    };
    List<Widget> tabWidgets = [];
    titles.forEach((key, value) {
      tabWidgets.add(Expanded(
        child: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            setState(() {
              _selectedTab = key;
            });
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(value, style: TextStyle(fontSize: 16, color: key == _selectedTab ? Theme.of(context).colorScheme.primaryTextColor : Theme.of(context).colorScheme.chipTextColor),),
              SizedBox(height: 8,),
              CustomDivider.zero(thickness: 2, color: key == _selectedTab ? Theme.of(context).primaryColor : Color(0xFFF2F2F2),),
            ],
          ),
        ),
      ),);
      if (key != titles.length - 1) {
        tabWidgets.add(SizedBox(width: 3,));
      }
    });
    return Row(children: [...tabWidgets],);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
                        padding: EdgeInsets.symmetric(horizontal: Constants.padding),

              child: Row(
                children: [
                  BaseAppBar(
                    title: 'Мои заказы',
                    onTapBack: () => Navigator.pop(context),
                  ),
                ],
              ),
            ),
            SizedBox(height: 31,),
            Padding(
                        padding: EdgeInsets.symmetric(horizontal: Constants.padding),

              child: _buildTabTitles(),
            ),
            Expanded(child: Padding(
                       padding: EdgeInsets.symmetric(horizontal: Constants.padding),

              child: SingleChildScrollView(child: Column(
                children: [
                  SizedBox(height: 37,),
                  _tabs[_selectedTab],
                ],
              ),),
            )),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
