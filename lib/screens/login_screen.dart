import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/access_recovery_screen.dart';
import 'package:mobile/screens/home_screen.dart';
import 'package:mobile/screens/registration_screen.dart';
import 'package:mobile/widgets/base/item_app_bar.dart';
import 'package:mobile/widgets/input/custom_text_input.dart';
import 'package:mobile/widgets/main_button.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: Container(
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Constants.padding),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 17,
                  ),
                  ItemAppBar(title: 'Вход'),
                  SizedBox(height: 64,),
                  SvgPicture.asset('assets/svg/logo.svg', width: MediaQuery.of(context).size.width * 0.6,),
                  SizedBox(height: 64,),
                  Text(
                    'Вход',
                    style: TextStyle(
                        fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 28,),
                  Text(
                    'Чтобы арендовать оборудование вам необходимо войти или зарегистрироваться',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16,
                      height: 1.34,
                      color: Theme.of(context).colorScheme.chipTextColor,
                    ),
                  ),
                  SizedBox(height: 25,),
                  CustomTextInput(
                      label: 'Номер телефона', hint: '+7 XXX XXX XXXX'),
                  SizedBox(height: 20,),
                  CustomTextInput(
                      label: 'Ваш пароль',
                      hint: '•  •  •  •  •  •  •  •  •  •  •  •  •'),
                  SizedBox(height: 17,),
                  GestureDetector(
                    onTap: () => Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_) => AccessRecoveryScreen())),
                    child: Text(
                      'Забыли пароль?',
                      style: TextStyle(
                        fontSize: 12,
                        color: Theme.of(context).colorScheme.chipTextColor,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ),
                  SizedBox(height: 19,),
                  MainButton(
                    text: 'Войти',
                    onPressed: () => Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_) => HomeScreen())),
                  ),
                  SizedBox(height: 21,),
                  MainButton(
                    text: 'Регистрация',
                    color: Theme.of(context).accentColor,
                    onPressed: () => Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_) => RegistrationScreen())),
                  ),
                  SizedBox(height: 22,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
