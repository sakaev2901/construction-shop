import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/screens/login_screen.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/user_data/company_tab.dart';
import 'package:mobile/widgets/user_data/docs_tab.dart';
import 'package:mobile/widgets/user_data/main_tab.dart';
import 'package:mobile/widgets/utils/swipe_detector.dart';

class UserDataScreen extends StatefulWidget {
  const UserDataScreen({Key? key}) : super(key: key);

  @override
  _UserDataScreenState createState() => _UserDataScreenState();
}

class _UserDataScreenState extends State<UserDataScreen> {
  int _selectedTab = 0;
  List<Widget> tabs = [
    MainTab(),
    CompanyTab(),
    DocumentsTab(),
  ];
  Offset _startOffset = Offset(0, 0);
  Offset _endOffset = Offset(0, 0);

  Widget _buildTabTitles() {
    Map<int, String> titles = {
      0: 'Основные',
      1: 'Компания',
      2: 'Документы',
    };
    List<Widget> tabWidgets = [];
    titles.forEach((key, value) {
      tabWidgets.add(
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              setState(() {
                _selectedTab = key;
              });
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  value,
                  style: TextStyle(
                      fontSize: 16,
                      color: key == _selectedTab
                          ? Theme.of(context).colorScheme.primaryTextColor
                          : Theme.of(context).colorScheme.chipTextColor),
                ),
                SizedBox(
                  height: 8,
                ),
                CustomDivider.zero(
                  thickness: 2,
                  color: key == _selectedTab
                      ? Theme.of(context).primaryColor
                      : Color(0xFFF2F2F2),
                ),
              ],
            ),
          ),
        ),
      );
      if (key != titles.length - 1) {
        tabWidgets.add(SizedBox(
          width: 3,
        ));
      }
    });
    return Row(
      children: [...tabWidgets],
    );
  }

  Widget _buildTab(int tabId) {
    switch (tabId) {
      case 0:
        return SwipeDetector(
          onSwipeLeft: () {
            setState(() {
              _selectedTab--;
            });
          },
          onSwipeRight: () {
            setState(() {
              _selectedTab++;
            });
          },
          child: MainTab(),
        );
      case 1:
        return SizedBox.expand(
          child: GestureDetector(
            onPanUpdate: (details) {
              print(details);
            },
            child: CompanyTab(),
          ),
        );
      case 2:
        return DocumentsTab();
      default:
        return MainTab();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Row(
                children: [
                  GestureDetector(
                    onPanUpdate: (details) {
                      print(details.delta);
                    },
                    child: BaseAppBar(
                      title: 'Мои данные',
                      onTapBack: () => Navigator.pop(context),
                    ),
                  ),
                  Spacer(),
                  GestureDetector(
                    onTap: () => Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_) => LoginScreen())),
                    child: Icon(
                      CustomIcons.logout,
                      size: 20,
                      color: Color(0xFFBEC1CC),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 31,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: _buildTabTitles(),
            ),
            Expanded(
                child: Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Listener(
                onPointerCancel: (event) {
                  print(event);
                },
                onPointerUp: (event) {
                  setState(() {
                    double deltaX = event.position.dx - _endOffset.dx;
                    double deltaY = event.position.dy - _endOffset.dy;
                    double allowableDeltaY = 20;
                    if (deltaX > 0 && deltaY.abs() < allowableDeltaY) {
                      if (_selectedTab > 0) {
                        _selectedTab--;
                      }
                    }
                    if (deltaX < 0 && deltaY.abs() < allowableDeltaY) {

                      if (_selectedTab < 2) {
                        _selectedTab++;
                      }
                    }
                  });
                },
                onPointerDown: (event) {
                  setState(() {
                    _endOffset = event.position;
                  });
                },
                child: SingleChildScrollView(
                  child: tabs[_selectedTab],
                ),
              ),
            )),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
