import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/models/category.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/search_widget.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/sub_category.dart';

class CategoryScreen extends StatefulWidget {
  GestureTapCallback? onTap;

  CategoryScreen({
    Key? key,
    this.onTap,
  }) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  bool _status = false;

  List<Widget> _buildCategories() {
    List<Widget> categoryWidgets = [];
    categoryWidgets.add(CustomDivider.zero());
    categories.forEach((element) {
      categoryWidgets.add(
        Container(
          padding: EdgeInsets.only(
            right: 15,
            top: 12,
            bottom: 12,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  setState(() {
                    element.isSelected = !element.isSelected;
                  });
                },
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Text(
                        element.name,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    // Spacer(),
                    Expanded(
                      child: Image.asset(
                        element.imageUrl,
                        height: 50,
                      ),
                    )
                  ],
                ),
              ),
              Visibility(
                visible: element.isSelected,
                child: Column(
                  children: [
                    ..._buildSubCategoryWidgets(element.subCategories),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
      categoryWidgets.add(CustomDivider.zero());
    });
    return categoryWidgets;
  }

  List<Widget> _buildSubCategoryWidgets(List<SubCategory> subCategories) {
    List<Widget> subCategoryWidgets = [];
    subCategories.forEach((element) {
      subCategoryWidgets.add(SubCategoryWidget(
        name: element.name,
        amount: element.amount,
        onTap: this.widget.onTap,
      ));
      subCategoryWidgets.add(SizedBox(
        height: 6,
      ));
    });
    return subCategoryWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        body: SafeArea(
          bottom: false,
          child: Padding(
        padding: EdgeInsets.only( top: 33),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Каталог',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        'Аренда',
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 14,
                          color: this._status
                              ? Theme.of(context).colorScheme.accentTextColor
                              : Theme.of(context).colorScheme.primaryTextColor,
                        ),
                      ),
                      SizedBox(
                        width: 12,
                      ),
                      FlutterSwitch(
                          value: _status,
                          height: 20,
                          width: 34,
                          toggleSize: 14,
                          padding: 3,
                          inactiveColor:
                              Theme.of(context).colorScheme.switchButtonColor,
                          activeColor:
                              Theme.of(context).colorScheme.switchButtonColor,
                          toggleColor: Theme.of(context).primaryColor,
                          borderRadius: Constants.borderRadius,
                          onToggle: (value) {
                            setState(
                              () {
                                _status = value;
                              },
                            );
                          }),
                      SizedBox(
                        width: 12,
                      ),
                      Text(
                        'Покупка',
                        style: TextStyle(
                          fontSize: 14,
                          color: !this._status
                              ? Theme.of(context).colorScheme.accentTextColor
                              : Theme.of(context).colorScheme.primaryTextColor,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 33,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Search(),
                      SizedBox(
                        height: 33,
                      ),
                      ..._buildCategories(),
                      SizedBox(
                        height: 40,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            NewNavBar(selectedPage: 1,),
          ],
        ),
      ),
    ));
  }
}
