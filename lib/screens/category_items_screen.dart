import 'package:drawing_animation/drawing_animation.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/models/sort_item.dart';
import 'package:mobile/screens/item_screen.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/cart/cart_bottom_sheet.dart';
import 'package:mobile/widgets/category_item.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/lazy_load_animation.dart';
import 'package:mobile/widgets/sort_bottom_sheet.dart';
import 'package:mobile/widgets/sort_bottom_sheet_item.dart';
import 'package:rive/rive.dart';

import 'filter_screen.dart';

class CategoryItemsScreen extends StatefulWidget {
  GestureTapCallback? onTap;
  GestureTapCallback? onTapItem;

  CategoryItemsScreen({Key? key, this.onTap, this.onTapItem}) : super(key: key);

  @override
  _CategoryItemsScreenState createState() => _CategoryItemsScreenState();
}

class _CategoryItemsScreenState extends State<CategoryItemsScreen> {
  late RiveAnimationController _controller;
  int _sortId = 0;
  bool run = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = SimpleAnimation('load');
    _controller.isActive = true;
  }

  void _showModal() {
    List<Widget> sheetChildren = [];
    sheetChildren.add(CustomDivider());
    sortItems.forEach((element) {
      sheetChildren.add(SortBottomSheetItem(
        name: element.name,
        isActive: _sortId == element.id,
        onTap: () {
          print(element.id);
          Navigator.pop(context);
          setState(() {
            _sortId = element.id;
          });
        },
      ));
      sheetChildren.add(CustomDivider());
    });
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          print('Hello');
          return Container(
            // height: MediaQuery.of(context).size.height * 0.4,
            // constraints: BoxConstraints(
            //   minHeight: 100,
            // ),
            color: Colors.transparent,
            child: SortingBottomSheet(
              children: [...sheetChildren],
            ),
          );
        });
  }

  void _showCartBottomSheet() {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) {
        return Container(
          color: Colors.transparent,
          child: CartBottomSheet(),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: BaseAppBar(
                title: 'Перфораторы',
                onTapBack: () => Navigator.pop(context),
              ),
            ),
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: CustomDivider.zero(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Row(
                children: [
                  CustomInkWell(
                    onTap: () => Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_) => FilterScreen())),
                    // onTap: () => Navigator.of(context).pushNamed('/filter'),
                    child: Container(
                      height: 50,
                      child: Row(
                        children: [
                          Icon(
                            CustomIcons.filter,
                            size: 16,
                            color:
                                Theme.of(context).colorScheme.selectedIconColor,
                          ),
                          SizedBox(
                            width: 14,
                          ),
                          Text(
                            'Фильтры',
                          )
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  CustomInkWell(
                    onTap: () => _showModal(),
                    child: Container(
                      height: 50,
                      child: Row(
                        children: [
                          Text(
                            sortItems[_sortId].name,
                          ),
                          SizedBox(
                            width: 14,
                          ),
                          Icon(
                            CustomIcons.swap,
                            size: 16,
                            color:
                                Theme.of(context).colorScheme.selectedIconColor,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: CustomDivider.zero(),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      CustomInkWell(
                          onTap: () => Navigator.of(context).push(
                              CupertinoPageRoute(
                                  builder: (_) => ItemScreen())),
                          child: CategoryItem(
                            onTap: _showCartBottomSheet,
                          )),
                      CustomDivider.zero(),
                       CustomInkWell(
                          onTap: () => Navigator.of(context).push(
                              CupertinoPageRoute(
                                  builder: (_) => ItemScreen())),
                          child: CategoryItem(
                            onTap: _showCartBottomSheet,
                          )),
                      CustomDivider.zero(),
                    CustomInkWell(
                          onTap: () => Navigator.of(context).push(
                              CupertinoPageRoute(
                                  builder: (_) => ItemScreen())),
                          child: CategoryItem(
                            onTap: _showCartBottomSheet,
                          )),
                      CustomDivider.zero(),
                     CustomInkWell(
                          onTap: () => Navigator.of(context).push(
                              CupertinoPageRoute(
                                  builder: (_) => ItemScreen())),
                          child: CategoryItem(
                            onTap: _showCartBottomSheet,
                          )),
                      CustomDivider.zero(),
                      SizedBox(
                        height: 28,
                      ),
                      LazyLoadAnimation(),
                      // SizedBox(
                      //   height: 14,
                      // ),
                      // Container(
                      //   width: 200,
                      //     height: 200,
                      //     child: RiveAnimation.asset('assets/svg/icon2.riv', animations: ['show'],))
                    ],
                  ),
                ),
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
