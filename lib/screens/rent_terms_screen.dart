import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/models/rent_terms.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/picker.dart';
import 'package:mobile/widgets/rent_term_card.dart';
import 'package:mobile/widgets/user_data/company_tab.dart';
import 'package:mobile/widgets/user_data/docs_tab.dart';
import 'package:mobile/widgets/user_data/main_tab.dart';

class RentTermsScreen extends StatefulWidget {
  const RentTermsScreen({Key? key}) : super(key: key);

  @override
  _RentTermsScreenState createState() => _RentTermsScreenState();
}

class _RentTermsScreenState extends State<RentTermsScreen> {

  int _selectedTab = 0;
  Offset _endOffset = Offset(0, 0);


  Widget _buildTabTitles() {
    Map<int, String> titles = {
      0: 'Для физ. лиц',
      1: 'Для юр. лиц',
    };
    List<Widget> tabWidgets = [];
    titles.forEach((key, value) {
      tabWidgets.add(
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              setState(() {
                _selectedTab = key;
              });
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  value,
                  style: TextStyle(
                      fontSize: 16,
                      color: key == _selectedTab
                          ? Theme.of(context).colorScheme.primaryTextColor
                          : Theme.of(context).colorScheme.chipTextColor),
                ),
                SizedBox(
                  height: 8,
                ),
                CustomDivider.zero(
                  thickness: 2,
                  color: key == _selectedTab
                      ? Theme.of(context).primaryColor
                      : Color(0xFFF2F2F2),
                ),
              ],
            ),
          ),
        ),
      );
      if (key != titles.length - 1) {
        tabWidgets.add(SizedBox(
          width: 3,
        ));
      }
    });
    return Row(
      children: [...tabWidgets],
    );
  }

  List<Widget> _buildTab(int tabId) {
    List<Widget> tabList = [];

    rentTerms[tabId].forEach((key, value) {
      tabList.add(RentTermCard(text: value, index: key));
      tabList.add(CustomDivider.zero());

    });

    return tabList;
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Row(
                children: [
                  BaseAppBar(
                    title: 'Условия аренды',
                    onTapBack: () => Navigator.pop(context),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 31,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: _buildTabTitles(),
            ),
            Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                  child: Listener(
                    onPointerCancel: (event) {
                      print(event);
                    },
                    onPointerUp: (event) {
                      setState(() {
                        double deltaX = event.position.dx - _endOffset.dx;
                        double deltaY = event.position.dy - _endOffset.dy;
                        double allowableDeltaY = 40;
                        if (deltaX > 0 && deltaY.abs() < allowableDeltaY) {
                          if (_selectedTab > 0) {
                            _selectedTab--;
                          }
                        }
                        if (deltaX < 0 && deltaY.abs() < allowableDeltaY) {

                          if (_selectedTab < 1) {
                            _selectedTab++;
                          }
                        }
                      });
                    },
                    onPointerDown: (event) {
                      setState(() {
                        _endOffset = event.position;
                      });
                    },
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          ..._buildTab(_selectedTab)
                        ],
                      ),
                    ),
                  ),
                )),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
