import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/cart_item.dart';
import 'package:mobile/widgets/cart_text_info.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/main_button.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 17,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Column(
                children: [
                  BaseAppBar(title: 'Заказ #5423543', onTapBack: () => Navigator.pop(context),),
                  Container(
                    transform: Matrix4.translationValues(0, -5, 0),
                    child: Row(
                      children: [
                        SizedBox(width: 55,),
                        Text('Создан 28 октября 2020', style: TextStyle(color: Theme.of(context).colorScheme.chipTextColor, fontSize: 12,),),
                      ],
                    ),
                  ),
                ],
              ),
            ),


            SizedBox(height: 20,),
            Expanded(child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: Column(
                  children: [
                    CustomDivider.zero(),
                    CartItem(isDisabled: true,),
                    CustomDivider.zero(),
                    CartItem(isDisabled: true,),
                    CustomDivider.zero(),
                    CartItem(isDisabled: true,),
                    CustomDivider.zero(),
                    CartItem(isDisabled: true,),
                  ],
                ),
              ),
            )),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: Column(
                children: [

                  CustomDivider.zero(thickness: 2,),
                  CartTextInfo(name: 'Товары • 2 шт', cost: '5 000 ₽'),
                  CustomDivider.zero(),
                  CartTextInfo(name: 'К оплате', cost: '5 500 ₽'),
                  CustomDivider.zero(),
                  CartTextInfo(name: 'Оплачено', cost: '1 500 ₽'),
                  CustomDivider.zero(),
                  CartTextInfo(name: 'Осталось оплатить', cost: '4 500 ₽', isDiscount: true, fontSize: 19,),
                  SizedBox(height: 20,),
                  Row(
                    children: [
                      Expanded(child: MainButton(text: 'Продлить', )),
                      SizedBox(width: 10,),
                      Expanded(child: MainButton(text: 'Отменить', textColor: Color(0xFF6B707B), color: Theme.of(context).accentColor,)),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(height: 23,),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
