import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/models/payment_chars.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/widgets/base/item_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class ServiceItemScreen extends StatefulWidget {
  bool isFavorite;

  ServiceItemScreen({Key? key, this.isFavorite = false}) : super(key: key);

  @override
  _ServiceItemScreenState createState() => _ServiceItemScreenState();
}

class _ServiceItemScreenState extends State<ServiceItemScreen> {
  List<Widget> _buildPaymentChars() {
    List<Widget> paymentCharsWidgets = [];
    paymentChars.forEach((key, value) {
      paymentCharsWidgets.add(Container(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Row(
          children: [
            Expanded(flex: 9, child: Text(key)),
            Spacer(),
            Expanded(
                flex: 1,
                child: SvgPicture.asset(
                  'assets/svg/$value',
                  width: 32,
                )),
            SizedBox(
              width: 10,
            ),
          ],
        ),
      ));
      paymentCharsWidgets.add(CustomDivider.zero());
    });
    return paymentCharsWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: ItemAppBar(
                onTapBack: () => Navigator.pop(context),
                title: "Услуги",
                secondButton: BaseIconButton(
                    icon: CustomIcons.upload,
                    iconSize: 20,
                    onTap: () {
                      setState(() {});
                    }),
              ),
            ),
            SizedBox(
              height: 13,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.36,
                      width: double.infinity,
                      child: ClipRRect(
                        child: Image.asset(
                          'assets/img/visor.png',
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(
                          Constants.borderRadius,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 29,
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: Constants.padding),
                      child: Column(
                        children: [
                          Text(
                            'Тепловизионное обследование',
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 7,
                          ),
                          Text(
                            'от 1 000 ₽',
                            style: TextStyle(fontSize: 20),
                          ),
                          SizedBox(
                            height: 22,
                          ),
                          CustomDivider.zero(),
                          SizedBox(
                            height: 24,
                          ),
                          Text(
                            'Выполняем обследование тепловизором квартир, домов и других обьектов. Ищем утечки тепла за короткий срок. Результатом тепловизионного исследования является сформированный отчет, в котором отражены текущие места утечек и рекомендации по их устранению.',
                            style: TextStyle(
                              fontSize: 16,
                              height: 1.64,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          CustomDivider.zero(),
                          SizedBox(height: 49),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Оплата',
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Theme.of(context)
                                      .colorScheme
                                      .chipTextColor,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 13),
                          ..._buildPaymentChars(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 17,
                bottom: 17,
                right: Constants.padding,
                left: Constants.padding,
              ),
              color: Colors.white,
              child: MainButton(
                onPressed: () => print('Main'),
                text: 'Заказать ремонт',
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
