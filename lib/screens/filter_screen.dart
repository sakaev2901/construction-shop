import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/models/filter_model.dart';
import 'package:mobile/models/filter_range.dart';
import 'package:mobile/models/filter_selected_range.dart';
import 'package:mobile/models/sort_item.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/filter/model_checkbox.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/widgets/slider/range_slider.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/sort_bottom_sheet.dart';
import 'package:mobile/widgets/sort_bottom_sheet_item.dart';

class FilterScreen extends StatefulWidget {
  FilterScreen({Key? key}) : super(key: key);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  initState() {
    filterRanges.forEach((element) {
      _filterSelectedRanges.add(FilterSelectedRange(
          max: element.max,
          min: element.min,
          name: element.name,
          upperValue: element.max,
          lowerValue: element.min,
          id: element.id));
    });
  }

  List<int> _checkedModels = [];
  List<FilterSelectedRange> _filterSelectedRanges = [];
  int _sortId = 0;

  List<Widget> _buildModelCheckboxes() {
    List<Widget> checkboxes = [];
    filterModels.forEach((element) {
      checkboxes.add(ModelCheckbox(
        amount: element.amount,
        model: element.name,
        isActive: _checkedModels.contains(element.id),
        onTap: () => _handleCheckedModelTap(element.id),
      ));
    });
    return checkboxes;
  }

  List<Widget> _buildRangeSliders() {
    List<Widget> rangeSliders = [];

    _filterSelectedRanges.forEach((element) {
      rangeSliders.add(
        CustomDivider(
          height: 32,
        ),
      );
      rangeSliders.add(
        Container(
          transform: Matrix4.translationValues(0, 5, 0),
          child: Text(
            element.name,
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
          ),
        ),
      );
      rangeSliders.add(
        CustomRangeSlider(
          min: element.min,
          max: element.max,
          upperValue: element.upperValue,
          lowerValue: element.lowerValue,
          onDragging: (handlerIndex, lowerValue, upperValue) {
            setState(() {
              element.lowerValue = lowerValue;
              element.upperValue = upperValue;
            });
          },
        ),
      );
    });
    return rangeSliders;
  }

  _handleCheckedModelTap(int id) {
    print(_checkedModels);
    setState(() {
      if (_checkedModels.any((element) => element == id)) {
        _checkedModels.remove(id);
      } else {
        _checkedModels.add(id);
      }
    });
  }

  void _showModal() {
    List<Widget> sheetChildren = [];
    sortItems.forEach((element) {
      sheetChildren.add(SortBottomSheetItem(
        name: element.name,
        isActive: _sortId == element.id,
        onTap: () {
          print(element.id);
          Navigator.pop(context);
          setState(() {
            _sortId = element.id;
          });
        },
      ));
    });
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          print('Hello');
          return Container(
            // height: MediaQuery.of(context).size.height * 0.4,
            // constraints: BoxConstraints(
            //   minHeight: 100,
            // ),
            color: Colors.transparent,
            child: SortingBottomSheet(
              children: [...sheetChildren],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Constants.padding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 17,
                ),
                Row(
                  children: [
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(Constants.borderRadius),
                        color: Theme.of(context).accentColor,
                      ),
                      child: CustomInkWell(
                        onTap: () => Navigator.pop(context),
                        borderRadius:
                            BorderRadius.circular(Constants.borderRadius),
                        child: Icon(
                          CustomIcons.close,
                          size: 16,
                          color:
                              Theme.of(context).colorScheme.selectedIconColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      'Фильтры',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Spacer(),
                    Container(
                      height: 40,
                      width: 69,
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius:
                            BorderRadius.circular(Constants.borderRadius),
                      ),
                      child: CustomInkWell(
                        borderRadius:
                            BorderRadius.circular(Constants.borderRadius),
                        onTap: () {
                          setState(() {
                            _checkedModels = [];
                            _filterSelectedRanges.forEach((element) {
                              element.upperValue = element.max;
                              element.lowerValue = element.min;
                            });
                          });
                        },
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Сброс',
                            style: TextStyle(
                                color: Theme.of(context)
                                    .colorScheme
                                    .accentTextColor,
                                fontSize: 14),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                ..._buildRangeSliders(),
                CustomDivider(
                  height: 32,
                ),
                Text(
                  'Марка',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 16,
                ),
                Wrap(
                  runSpacing: 6,
                  children: [
                    ..._buildModelCheckboxes(),
                  ],
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(
            left: Constants.padding, right: Constants.padding, bottom: 10),
        child: MainButton(
          onPressed: () => Navigator.pop(context),
          text: 'Показать 16 товаров',
        ),
      ),
    );
  }
}
