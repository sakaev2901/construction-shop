import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/login_screen.dart';
import 'package:mobile/widgets/input/custom_text_input.dart';
import 'package:mobile/widgets/main_button.dart';

import 'check_registration_request_screen.dart';

class AccessRecoveryScreen extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: Constants.padding),
          child: Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height - 50),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset('assets/svg/access.svg'),
                            SizedBox(height: 48,),
                            Text('Восстановление доступа', textAlign: TextAlign.center, style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
                            SizedBox(height: 24,),
                            Text('Для восстановления доступа\nвведите адрес электронной почты\nна который мы отправим ссылку\nдля восстановления пароля.',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 16, height: 1.34, color: Theme.of(context).colorScheme.chipTextColor,),
                            ),
                            SizedBox(height: 36,),
                            CustomTextInput(label: 'Электронная почта', hint: 'name@mail.ru'),
                            SizedBox(height: 23,),
                            MainButton(text: 'Отправить',   onPressed: () => Navigator.of(context).push(
                                CupertinoPageRoute(builder: (_) => SuccessRequestScreen(barTitle: 'Восстановить пароль', mainTitle: 'Отлично!\nМы отправили ссылку', text: 'Проверьте вашу электронную почту\nи выполните восстановление\nпо ссылке в письме.',))),)
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Positioned(left: 0, right: 0, top: 17,child: Container(color:Colors.white,child: Text('Восстановить пароль', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.w700, color: Theme.of(context).colorScheme.secondaryTextColor,),)))
            ],
          ),
        ),
      ),
    );
  }
}
