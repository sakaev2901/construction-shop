import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/about_screen.dart';
import 'package:mobile/screens/finances_screen.dart';
import 'package:mobile/screens/orders_list_screen.dart';
import 'package:mobile/screens/payment_delivery_screen.dart';
import 'package:mobile/screens/pick_up_points_screen.dart';
import 'package:mobile/screens/rent_terms_screen.dart';
import 'package:mobile/screens/user_data_screen.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/profile_chip.dart';
import 'package:mobile/widgets/profile_navigation_item.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: Column(
                  children: [
                    SizedBox(
                      height: 29,
                    ),
                    Container(
                      child: CustomInkWell(
                        onTap: () => Navigator.of(context).push(
                            CupertinoPageRoute(builder: (_) => UserDataScreen())),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Constants.borderRadius),
                                color: Theme.of(context).primaryColor,
                              ),
                              child: Icon(
                                CustomIcons.profile_7_1,
                                size: 20,
                                color:
                                    Theme.of(context).colorScheme.selectedIconColor,
                              ),
                            ),
                            SizedBox(
                              width: 16,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Text(
                                  'Александр Кузичев',
                                  style: TextStyle(fontSize: 16),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'Мои данные',
                                  style: TextStyle(
                                    fontSize: 12,
                                    color:
                                        Theme.of(context).colorScheme.chipTextColor,
                                  ),
                                )
                              ],
                            ),
                            Spacer(),
                            Icon(
                              CustomIcons.arrow_right,
                              size: 16,
                              color: Color(0xFF979BA5),
                            ),
                            SizedBox(
                              width: 14,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: ProfileChip(
                                iconData: CustomIcons.wallet,
                                title: '84 000 ₽',
                                subTitle: 'Лицевой счет',
                                color: Theme.of(context).accentColor)),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: ProfileChip(
                                iconData: CustomIcons.discount,
                                title: 'Скидка 5%',
                                subTitle: 'Ваш уровень',
                                color: Theme.of(context).accentColor)),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: ProfileChip(
                                iconData: CustomIcons.star,
                                title: '1 800',
                                subTitle: 'Бонусных баллов',
                                color: Color(0xFFE9FFB9))),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            child: ProfileChip(
                                iconData: CustomIcons.infocircle,
                                title: '12 500 ₽',
                                subTitle: 'Задолженность',
                                color: Color(0xFFFFDBDB))),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ProfileNavigationItem(
                      onTap: () => Navigator.of(context).push(
                          CupertinoPageRoute(builder: (_) => OrdersListScreen())),
                      iconData: CustomIcons.buy_6_1,
                      title: 'Мои заказы',
                      badgeCount: 3,
                    ),
                    CustomDivider.zero(),
                    ProfileNavigationItem(
                        onTap: () => Navigator.of(context).push(
                            CupertinoPageRoute(builder: (_) => FinancesScreen())),
                        iconData: CustomIcons.wallet, title: 'Финансы'),
                    CustomDivider.zero(),
                    ProfileNavigationItem(
                        onTap: () => Navigator.of(context).push(
                            CupertinoPageRoute(builder: (_) => RentTermsScreen())),
                        iconData: CustomIcons.cahrt, title: 'Условия аренды'),
                    CustomDivider.zero(),
                    ProfileNavigationItem(
                        onTap: () => Navigator.of(context).push(
                            CupertinoPageRoute(builder: (_) => PaymentDeliveryScreen())),
                        iconData: CustomIcons.setting, title: 'Оплата и доставка'),
                    CustomDivider.zero(),
                    ProfileNavigationItem(
                        onTap: () => Navigator.of(context).push(
                            CupertinoPageRoute(builder: (_) => PickUpPointsScreen())),
                        iconData: CustomIcons.location, title: 'Точки самовызова'),
                    CustomDivider.zero(),
                    ProfileNavigationItem(
                        onTap: () => Navigator.of(context).push(
                            CupertinoPageRoute(builder: (_) => AboutScreen())),
                        iconData: CustomIcons.more, title: 'О компании'),
                  ],
                ),
              ),
            ),
            NewNavBar(selectedPage: 4,),
          ],
        ),
      ),
    );
  }
}
