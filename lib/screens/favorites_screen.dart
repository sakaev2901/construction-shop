import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/category_item.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class FavoritesScreen extends StatelessWidget {
  GestureTapCallback? onTap;
  FavoritesScreen({Key? key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: Constants.padding),
              child: BaseAppBar(
                title: 'Избранное',
                onTapBack: () => Navigator.of(context).pop(),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            CustomDivider.zero(),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      CustomInkWell(
                          child: CategoryItem(
                        isFavorite: true,
                      )),
                      CustomDivider.zero(),
                      CustomInkWell(
                          child: CategoryItem(
                        isFavorite: true,
                      )),
                      CustomDivider.zero(),
                      CustomInkWell(
                          child: CategoryItem(
                        isFavorite: true,
                      )),
                      CustomDivider.zero(),
                      CustomInkWell(
                          child: CategoryItem(
                        isFavorite: true,
                      )),
                      CustomDivider.zero(),
                      CustomInkWell(
                          child: CategoryItem(
                        isFavorite: true,
                      )),
                      CustomDivider.zero(),
                    ],
                  ),
                ),
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
