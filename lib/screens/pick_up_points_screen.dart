import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/base/base_app_bar.dart';
import 'package:mobile/widgets/base/base_icon_button.dart';
import 'package:mobile/widgets/base/item_app_bar.dart';
import 'package:mobile/widgets/bottom_nav_bar/new_bottom_nav_bar.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/pick_up_point.dart';

class PickUpPointsScreen extends StatefulWidget {
  const PickUpPointsScreen({Key? key}) : super(key: key);

  @override
  _PickUpPointsScreenState createState() => _PickUpPointsScreenState();
}

class _PickUpPointsScreenState extends State<PickUpPointsScreen> {
  int activeMapIndex = -1;

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(55.797263945006584, 49.11133995065905),
    zoom: 17,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  // Completer<GoogleMapController> _controller = Completer();
  late GoogleMapController _controller;

  late BitmapDescriptor customIcon;

  @override
  void initState() {
    super.initState();
    print('hello');
    setCustomMarker();
  }

  void setCustomMarker() async {
    setState(() {});
    customIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 1,
        ),
        'assets/img/location3.png');
    markers.add(Marker(
      markerId: MarkerId('1'),
      position: LatLng(55.797263945006584, 49.11133995065905),
      infoWindow: InfoWindow(title: 'La Sagrada Familia'),
      icon: customIcon,
    ));
  }

  Set<Marker> markers = {};

  @override
  Widget build(BuildContext context) {
    final GoogleMap googleMap = GoogleMap(
      mapType: MapType.normal,
      markers: markers,
      initialCameraPosition: _kGooglePlex,
      zoomControlsEnabled: true,
      onMapCreated: (GoogleMapController controller) {
        this._controller = controller;
      },
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            SizedBox(
              height: 17,
            ),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: BaseAppBar(
                  title: 'Точки самовызова',
                  onTapBack: () => Navigator.pop(context),
                )),
            SizedBox(
              height: 13,
            ),
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(horizontal: Constants.padding),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 14,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: 16, bottom: 18, top: 18, right: 16),
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius:
                            BorderRadius.circular(Constants.borderRadius),
                      ),
                      child: Row(
                        children: [
                          Icon(
                            CustomIcons.location,
                            size: 18,
                            color:
                                Theme.of(context).colorScheme.selectedIconColor,
                          ),
                          SizedBox(
                            width: 13,
                          ),
                          Text('Казань'),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    CustomInkWell(
                        borderRadius:
                            BorderRadius.circular(Constants.borderRadius),
                        onTap: () {
                          setState(() {
                            if (activeMapIndex == 0) {
                              activeMapIndex = -1;
                            } else {
                              activeMapIndex = 0;
                            }
                          });
                        },
                        child: PickUpPoint(
                          googleMap: googleMap,
                          isOpen: activeMapIndex == 0,
                        )),
                    SizedBox(
                      height: 18,
                    ),
                    CustomInkWell(
                        borderRadius:
                        BorderRadius.circular(Constants.borderRadius),
                        onTap: () {
                          setState(() {
                            if (activeMapIndex == 1) {
                              activeMapIndex = -1;
                            } else {
                              activeMapIndex = 1;
                            }
                          });
                        },
                        child: PickUpPoint(
                          googleMap: googleMap,
                          isOpen: activeMapIndex == 1,
                        )),
                    SizedBox(
                      height: 18,
                    ),
                    CustomInkWell(
                        borderRadius:
                        BorderRadius.circular(Constants.borderRadius),
                        onTap: () {
                          setState(() {
                            if (activeMapIndex == 2) {
                              activeMapIndex = -1;
                            } else {
                              activeMapIndex = 2;
                            }
                          });
                        },
                        child: PickUpPoint(
                          googleMap: googleMap,
                          isOpen: activeMapIndex == 2,
                        )),
                  ],
                ),
              ),
            ),
            NewNavBar(),
          ],
        ),
      ),
    );
  }
}
