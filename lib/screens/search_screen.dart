import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/category_item.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/lazy_load_animation.dart';
import 'package:mobile/widgets/search_chip.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(top: 17, left: Constants.padding, right: Constants.padding,),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Container(
                      height: 54,
                      padding: EdgeInsets.only(right: 16),
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(Constants.borderRadius),
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Icon(
                              CustomIcons.search,
                              size: 18,
                              color: Theme.of(context).colorScheme.selectedIconColor,
                            ),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(bottom: 3),
                              child: TextField(
                                enabled: true,
                                style: TextStyle(
                                  color: Theme.of(context).colorScheme.selectedIconColor,
                                  fontSize: 14,
                                ),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Поиск по городу',
                                  hintStyle: TextStyle(
                                    color: Theme.of(context).colorScheme.accentTextColor,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  CustomInkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      height: 54,
                      width: 38,
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        borderRadius: BorderRadius.circular(Constants.borderRadius)
                      ),
                      child: Icon(CustomIcons.close, size: 16, color: Theme.of(context).colorScheme.selectedIconColor,),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 21,),
              Wrap(
                spacing: 8,
                runSpacing: 8,
                crossAxisAlignment: WrapCrossAlignment.start,
                children: [
                  SearchChip(itemName: 'Перфоратор'),
                  SearchChip(itemName: 'Перфоратор Mockita'),
                  SearchChip(itemName: 'Перфоратор'),
                ],
              ),
              SizedBox(height: 21,),
              CustomDivider.zero(),
              CustomInkWell(child: CategoryItem()),
              CustomDivider.zero(),
              CustomInkWell(child: CategoryItem()),
              CustomDivider.zero(),
              SizedBox(
                height: 28,
              ),
              LazyLoadAnimation(),
            ],
          ),
        ),
      ),
    );
  }
}
