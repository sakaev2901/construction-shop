class Constants {
  static final double borderRadius = 12;
  static final double padding = 17;
  static final double checkboxBorderRadius = 5;
}