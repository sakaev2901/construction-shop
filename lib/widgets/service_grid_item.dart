import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';

class ServiceGridItem extends StatelessWidget {
  final String name;
  final int startingCost;
  final String imageUrl;

  const ServiceGridItem(
      {Key? key,
      required this.name,
      required this.imageUrl,
      required this.startingCost})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        ClipRRect(
          child: Image.asset(
            this.imageUrl,
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.circular(Constants.borderRadius),
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
              color: Colors.white,
              gradient: LinearGradient(
                  begin: FractionalOffset.topCenter,
                  end: FractionalOffset.bottomCenter,
                  colors: [
                    Color.fromRGBO(28, 31, 39, 0),
                    Color.fromRGBO(28, 31, 39, 0.8),
                  ],
                  stops: [
                    0.0,
                    1.0
                  ])),
        ),
        Positioned(
          bottom: 16,
          left: 16,
          right: 16,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                // padding: EdgeInsets.all(5),
                child: Text(
                  this.name,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(5),
                ),
                padding: EdgeInsets.all(5),
                child: Text(
                  'от ${this.startingCost} ₽',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),

            ],
          ),
        ),
      ],
    );
  }
}
