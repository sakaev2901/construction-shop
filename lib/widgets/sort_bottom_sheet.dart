import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/sort_bottom_sheet_item.dart';

import 'custom_divider.dart';
import 'custom_ink_well.dart';

class SortingBottomSheet extends StatelessWidget {
  final List<Widget> children;

  const SortingBottomSheet({Key? key,required this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(24), topRight: Radius.circular(24),),
      ),
      padding: EdgeInsets.only(
          left: Constants.padding, right: Constants.padding, bottom: 20),
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 30,
                  ),
                  Text(
                    'Сортировка',
                    style: TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.opaque,
                    child: Container(
                      padding: EdgeInsets.only(left: 20),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          CustomIcons.close,
                          size: 16,
                          color: Theme.of(context)
                              .colorScheme
                              .selectedIconColor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
         ...children,
        ],
      ),
    );
  }
}
