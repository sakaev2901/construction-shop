import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class ProfileNavigationItem extends StatelessWidget {
  
  final IconData iconData;
  final String title;
  int? badgeCount;
  GestureTapCallback? onTap;
  
  ProfileNavigationItem({Key? key,this.onTap, required this.iconData, required this.title, this.badgeCount}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: this.onTap,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 17, top: 17, left: 16, right: 9),
        child: Row(
          children: [
            Icon(this.iconData, size: 18, color: Theme.of(context).colorScheme.selectedIconColor,),
            SizedBox(width: 15,),
            Text(this.title),
            Spacer(),
            Visibility(
              visible: this.badgeCount != null,
              child: Container(
                height: 18,
                width: 18,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(9),
                  color: Theme.of(context).primaryColor,
                ),
                child: Align(
                  alignment: Alignment.center,
                  child: Text('${this.badgeCount}', style: TextStyle(fontWeight: FontWeight.w700, height: 1.3),),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
