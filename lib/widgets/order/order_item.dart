import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/order_screen.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class OrderItem extends StatefulWidget {
  final int cost;
  final String date;
  final int orderId;
  final List<String> orders;
  final String status;
  final int rentDayCount;

  const OrderItem(
      {Key? key,
      required this.cost,
      required this.date,
      required this.orderId,
      required this.orders,
      this.status = 'ЗАВЕРШЕН',
      required this.rentDayCount})
      : super(key: key);

  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  Widget _buildOrderItemsList() {
    List<Widget> widgets = [];
    for (int i = 0; i < this.widget.orders.length; i++) {
      widgets.add(
          Expanded(flex: 2,child: Image.asset('assets/img/${this.widget.orders[i]}', fit: BoxFit.contain,))
      );
      widgets.add(SizedBox(width: 10,),);
      if (i == 2) break;
    }
    for (int i = this.widget.orders.length; i < 3; i++) {
      widgets.add(
          Expanded(flex: 2,
              child: SizedBox())
      );
      widgets.add(SizedBox(width: 10,),);
    }
    if (this.widget.orders.length > 3) {
      widgets.add(
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Theme.of(context).accentColor,
            ),
            child: Align(
                alignment: Alignment.center,
                child: Text(
                  'Еще ${this.widget.orders.length - 3}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF9499A6)),
                )),
          ),
        ),
      );
    }
    return Container(
      height: 80,
      child: Row(
        children: [...widgets],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
        onTap: () => Navigator.of(context).push(
            CupertinoPageRoute(builder: (_) => OrderScreen())),
      child: Container(
        padding: EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Constants.borderRadius),
          border: Border.all(color: Theme.of(context).accentColor, width: 1),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '${this.widget.cost} ₽  •  Создан ${this.widget.date}',
              style: TextStyle(
                fontSize: 12,
                color: Theme.of(context).colorScheme.chipTextColor,
              ),
            ),
            SizedBox(height: 8,),
            Text(
              'Заказ на аренду #${this.widget.orderId}',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8,),
            _buildOrderItemsList(),
            SizedBox(height: 10,),
            Row(
              children: [
                Container(
                  padding: EdgeInsets.only(
                    top: 5,
                    bottom: 5,
                    left: 15,
                    right: 15,
                  ),
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(this.widget.status, style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  'Аренда на ${this.widget.rentDayCount} дней',
                  style: TextStyle(
                    fontSize: 12,
                    color: Theme.of(context).colorScheme.chipTextColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
