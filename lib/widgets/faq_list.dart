import 'package:flutter/material.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/screens/location_screen.dart';
import 'package:mobile/screens/payment_delivery_screen.dart';
import 'package:mobile/screens/pick_up_points_screen.dart';
import 'package:mobile/screens/rent_terms_screen.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/faq_list_item.dart';

class FAQList extends StatelessWidget {
  const FAQList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          CustomDivider.zero(),
          FAQListItem(text: 'Условия аренды', icon: CustomIcons.cahrt, screen: RentTermsScreen(),),
          CustomDivider.zero(),
          FAQListItem(text: 'Оплата и доставка', icon: CustomIcons.setting, screen: PaymentDeliveryScreen(),),
          CustomDivider.zero(),
          FAQListItem(text: 'Точки самовызова', icon: CustomIcons.location, screen: PickUpPointsScreen(),),
          CustomDivider.zero(),
        ],
      ),
    );
  }
}
