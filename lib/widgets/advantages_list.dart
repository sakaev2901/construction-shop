import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/widgets/advatage_card.dart';

class AdvantagesList extends StatelessWidget {
  const AdvantagesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(
        childAspectRatio: 1.1,
        primary: false,
        shrinkWrap: true,
        crossAxisCount: 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        children: [
          AdvantageCard(imagePath: 'assets/svg/adv1.svg', text: 'Доставка \nвсего от 150 ₽ \nпо городу'),
          AdvantageCard(imagePath: 'assets/svg/adv2.svg', text: 'Без залога \nна получение \nоборудования'),
          AdvantageCard(imagePath: 'assets/svg/adv3.svg', text: 'Варианты \nоплаты \nс НДС и без'),
          AdvantageCard(imagePath: 'assets/svg/adv4.svg', text: 'Скидка 10% \nна первую аренду \nоборудования'),
        ],
      ),
    );
  }
}
