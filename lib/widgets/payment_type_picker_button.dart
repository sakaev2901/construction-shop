import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class PaymentTypePickerButton extends StatelessWidget {
  final String name;
  final int index;
  Function? onTap;
  bool isActive;

  PaymentTypePickerButton({Key? key, required this.name, this.isActive = false, this.onTap, required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: () => this.onTap!(index),
      child: Container(
        height: 30,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: this.isActive ? Theme.of(context).colorScheme.activeButtonColor : Colors.white,
        ),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            this.name,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: this.isActive ? Colors.white : Theme.of(context).colorScheme.primaryTextColor,
            ),
          ),
        ),
      ),
    );
  }
}
