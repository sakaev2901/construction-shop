import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class CartTextInfo extends StatelessWidget {
  final String name;
  final String cost;
  bool isDiscount;
  double fontSize;

  CartTextInfo({
    Key? key,
    required this.name,
    required this.cost,
    this.isDiscount = false,
    this.fontSize = 14,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10, bottom: 10, right: 8),
      child: Row(
        children: [
          Text(
            this.name,
            style: TextStyle(
              fontSize: this.fontSize,
              color: !this.isDiscount
                  ? Theme.of(context).colorScheme.primaryTextColor
                  : Theme.of(context).colorScheme.discountTextColor,
            ),
          ),
          Spacer(),
          Text(
            this.cost,
            style: TextStyle(
              fontSize: this.fontSize,
              fontWeight: FontWeight.bold,
              color: !this.isDiscount
                  ? Theme.of(context).colorScheme.primaryTextColor
                  : Theme.of(context).colorScheme.discountTextColor,
            ),
          ),
        ],
      ),
    );
  }
}
