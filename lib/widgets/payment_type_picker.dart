import 'package:flutter/material.dart';
import 'package:mobile/widgets/payment_type_picker_button.dart';

class PaymentTypePicker extends StatelessWidget {
  Function? onTap;
  final int activeIndex;

  PaymentTypePicker({Key? key, this.onTap, required this.activeIndex}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.circular(9),
      ),
      child: Row(
        children: [
          Expanded(child: PaymentTypePickerButton(name: 'Картой', isActive: activeIndex == 0, index: 0, onTap: this.onTap,)),
          SizedBox(width: 4,),
          Expanded(child: PaymentTypePickerButton(name: 'Наличные', isActive: activeIndex == 1, index: 1, onTap: this.onTap,)),
          SizedBox(width: 4,),
          Expanded(child: PaymentTypePickerButton(name: 'На счет', isActive: activeIndex == 2, index: 2, onTap: this.onTap,)),
        ],
      ),
    );
  }
}
