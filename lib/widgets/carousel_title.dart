import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';

class CarouselTitle extends StatelessWidget {
  final String title;

  CarouselTitle({required this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: Constants.padding),
      child: Text(
        this.title,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
      ),
    );
  }
}
