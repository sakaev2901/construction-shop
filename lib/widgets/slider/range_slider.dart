import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

class CustomRangeSlider extends StatefulWidget {

  final double lowerValue;
  final double upperValue;
  final double max;
  final double min;
  final Function(int handlerIndex, dynamic lowerValue, dynamic upperValue) onDragging;

  CustomRangeSlider(
      {Key? key, required this.lowerValue, required this.upperValue, required this.max, required this.min, required this.onDragging})
      : super(key: key);

  @override
  _CustomRangeSliderState createState() => _CustomRangeSliderState();
}

class _CustomRangeSliderState extends State<CustomRangeSlider> {


  @override
  Widget build(BuildContext context) {
    final FlutterSliderHandler handler = FlutterSliderHandler(
      decoration: BoxDecoration(
        color: Theme
            .of(context)
            .primaryColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Container(
        height: 16,
        width: 16,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
      ),
    );

    return Column(
      children: [
        FlutterSlider(
          rangeSlider: true,
          touchSize: 15,
          values: [widget.lowerValue, widget.upperValue],
          min: widget.min,
          max: widget.max,
          handlerHeight: 24,
          handler: handler,
          tooltip: FlutterSliderTooltip(
            disabled: true,
          ),
          onDragging: this.widget.onDragging,
          rightHandler: handler,
          handlerAnimation: FlutterSliderHandlerAnimation(scale: 1),
          trackBar: FlutterSliderTrackBar(
            inactiveTrackBar: BoxDecoration(
              color: Color(0xFFF2F2F2),
            ),
            activeTrackBar: BoxDecoration(
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
            activeTrackBarHeight: 4,
            inactiveTrackBarHeight: 4,
          ),
        ),
        Container(
          transform: Matrix4.translationValues(0, -5, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('От ${widget.lowerValue.toInt()}', style: TextStyle(fontSize: 12,),),
              Text('До ${widget.upperValue.toInt()}', style: TextStyle(fontSize: 12,),),
            ],
          ),
        )
      ],
    );
  }
}
