import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/input/input_type.dart';
import 'package:mobile/widgets/input/masked_text_input_formatter.dart';

class CustomTextInput extends StatefulWidget {

  final InputType inputType;
  final String label;
  final String hint;
  double bottomMargin;
  String value = '';

  CustomTextInput({Key? key,required this.label, required this.hint, this.bottomMargin = 0, this.inputType = InputType.STRING}) : super(key: key);

  @override
  _CustomTextInputState createState() => _CustomTextInputState();
}

class _CustomTextInputState extends State<CustomTextInput> {
  String value = '';
  String obscureText = '';

  var _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 14, top: 10, bottom: 10, right: 14,),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9),
            border: Border.all(color: Theme.of(context).accentColor, width: 1),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(this.widget.label, style: TextStyle(fontSize: 12, color: this.value != '' ? Theme.of(context).colorScheme.chipTextColor : Theme.of(context).colorScheme.primaryTextColor,),),
              SizedBox(height: 9,),
              Container(
                child: TextField(
                  controller: _textController,
                  onChanged: (val) {
                    setState(() {
                      // this.obscureText += '•';
                      // _textController.text = this.obscureText;
                      this.value = val;
                    });
                  },
                  enabled: true,
                  // inputFormatters: [MaskedTextInputFormatter(mask: 'xxxx', separator: '')],
                  style: TextStyle(
                    fontSize: 16,
                  ),
                  // keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.symmetric(vertical: 0),
                    hintText: widget.hint,
                    hintStyle: TextStyle(
                      color: Theme.of(context).colorScheme.accentTextColor,
                      fontSize: 16,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: this.widget.bottomMargin,),
      ],
    );
  }
}
