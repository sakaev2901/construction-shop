import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/screens/category_items_screen.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class SubCategoryWidget extends StatelessWidget {
  final String name;
  final int amount;
  GestureTapCallback? onTap;

  SubCategoryWidget(
      {Key? key, required this.name, required this.amount, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: () => Navigator.of(context)
          .push(CupertinoPageRoute(builder: (_) => CategoryItemsScreen())),
      child: Container(
        height: 34,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Constants.borderRadius),
          border: Border.all(color: Theme.of(context).accentColor),
          color: Theme.of(context).accentColor,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              Text(
                name,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Spacer(),
              Text(
                '$amount',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: Theme.of(context).colorScheme.chipTextColor,
                ),
              ),
              SizedBox(
                width: 11,
              ),
              Icon(
                CustomIcons.arrow_right,
                color: Theme.of(context).colorScheme.selectedIconColor,
                size: 12,
              )
            ],
          ),
        ),
      ),
    );
  }
}
