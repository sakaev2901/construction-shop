import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/screens/favorites_screen.dart';
import 'package:mobile/screens/location_screen.dart';
import 'package:mobile/screens/search_screen.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class Search extends StatelessWidget {
  GestureTapCallback? onTapFavorites;
  Search({Key? key, this.onTapFavorites}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 5,
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () => Navigator.of(context).push(CupertinoPageRoute(builder: (_) => SearchScreen())),
              child: Container(
                height: 54,
                padding: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(Constants.borderRadius),
                ),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Icon(
                        CustomIcons.search,
                        size: 18,
                        color: Theme.of(context).colorScheme.selectedIconColor,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(bottom: 3),
                        child: TextField(
                          enabled: false,
                          style: TextStyle(
                            color: Theme.of(context).colorScheme.selectedIconColor,
                            fontSize: 14,
                          ),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Поиск по каталогу',
                            hintStyle: TextStyle(
                              color: Theme.of(context).colorScheme.accentTextColor,
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 9,
          ),
          Expanded(
            child: CustomInkWell(
              onTap:() => Navigator.of(context).push(CupertinoPageRoute(builder: (_) => LocationScreen())),
              child: Container(
                height: 54,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(Constants.borderRadius),
                ),
                child: Icon(
                  CustomIcons.location,
                  size: 18,
                  color: Theme.of(context).colorScheme.selectedIconColor,
                ),
              ),
            ),
          ),
          SizedBox(
            width: 9,
          ),
          Expanded(
            child: CustomInkWell(
              onTap: () => Navigator.of(context).push(CupertinoPageRoute(builder: (_) => FavoritesScreen())),
              child: Container(
                height: 54,
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(Constants.borderRadius),
                ),
                child: Icon(
                  CustomIcons.star,
                  size: 18,
                  color: Theme.of(context).colorScheme.selectedIconColor,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
