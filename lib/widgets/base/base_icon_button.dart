import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

import '../custom_ink_well.dart';

class BaseIconButton extends StatelessWidget {
  GestureTapCallback? onTap;
  final IconData icon;
  final int iconSize;

  BaseIconButton({Key? key,required this.icon, this.iconSize = 20, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
        borderRadius:
        BorderRadius.circular(Constants.borderRadius),
        color: Theme.of(context).accentColor,
      ),
      child: CustomInkWell(
        onTap: this.onTap,
        borderRadius:
        BorderRadius.circular(Constants.borderRadius),
        child: Icon(
          this.icon,
          size: this.iconSize.toDouble(),
          color:
          Theme.of(context).colorScheme.selectedIconColor,
        ),
      ),
    );
  }
}
