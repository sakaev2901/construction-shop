import 'package:flutter/material.dart';
import 'package:mobile/icons/custom_icons_icons.dart';

import 'base_icon_button.dart';

class BaseAppBar extends StatelessWidget {

  final String title;
  GestureTapCallback? onTapBack;

  BaseAppBar({Key? key, required this.title, this.onTapBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        BaseIconButton(
          onTap: this.onTapBack,
          icon: CustomIcons.arrow_left,
          iconSize: 16,
        ),
        SizedBox(
          width: 15,
        ),
        Text(
          this.title,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
