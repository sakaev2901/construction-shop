import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:mobile/widgets/main_button.dart';

class CategoryItem extends StatefulWidget {
  GestureTapCallback? onTap;
  bool isFavorite;
  CategoryItem({Key? key, this.onTap, this.isFavorite = false}) : super(key: key);

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 166,
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Expanded(flex: 2,child: Image.asset('assets/img/item.png')),
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Перфоратор Makita HR2450', style: TextStyle(fontSize: 12),),
                SizedBox(height: 12,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text('300 ₽', style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,),),
                    SizedBox(width: 4,),
                    Text('/сутки', style: TextStyle(fontSize: 10, fontWeight: FontWeight.bold),)
                  ],
                ),
                Spacer(),
                Row(
                  children: [
                    CustomInkWell(
                      onTap: this.widget.onTap,
                      child: Container(
                        height: 36,
                        width: 100,
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(Constants.borderRadius),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text('В корзину'),
                        ),
                      ),
                    ),
                    Spacer(),
                    // IconButton(onPressed:  () {
                    //   setState(() {
                    //     this.widget.isFavorite = !this.widget.isFavorite;
                    //   });
                    // }, icon: Icon(this.widget.isFavorite ? CustomIcons.star_filled : CustomIcons.star, size: 20,)),
                    CustomInkWell(onTap: () {
                      setState(() {
                        this.widget.isFavorite = !this.widget.isFavorite;
                      });
                    },child: Icon(this.widget.isFavorite ? CustomIcons.star_filled : CustomIcons.star, size: 20,)),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
