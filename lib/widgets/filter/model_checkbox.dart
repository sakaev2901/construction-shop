import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';

class ModelCheckbox extends StatelessWidget {
  bool isActive;
  final String model;
  final int amount;
  GestureTapCallback? onTap;

  ModelCheckbox(
      {this.isActive = false,
      required this.amount,
      required this.model,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      child: Container(
        height: 34,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Constants.borderRadius),
          border: Border.all(color: Theme.of(context).accentColor),
          color: this.isActive ? Theme.of(context).accentColor : Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 13, right: 8),
          child: Row(
            children: [
              Text(
                model,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: this.isActive
                      ? Theme.of(context).textTheme.bodyText2?.color
                      : Theme.of(context).colorScheme.secondaryTextColor,
                ),
              ),
              Spacer(),
              Text(
                '$amount',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                  color: Theme.of(context).colorScheme.chipTextColor,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                height: 18,
                width: 18,
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.circular(Constants.checkboxBorderRadius),
                    border: Border.all(
                        color: this.isActive
                            ? Theme.of(context).colorScheme.selectedIconColor
                            : Theme.of(context)
                                .colorScheme
                                .checkboxBorderColor)),
                child: Visibility(
                  child: Icon(
                    CustomIcons.success,
                    size: 11,
                    color: Theme.of(context).colorScheme.selectedIconColor,
                  ),
                  visible: this.isActive,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
