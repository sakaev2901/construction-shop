import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobile/widgets/map/zoom_button.dart';

class PickUpPoint extends StatefulWidget {
  bool isOpen = false;
  final GoogleMap googleMap;

  PickUpPoint({required this.googleMap, this.isOpen = false});

  @override
  _PickUpPointState createState() => _PickUpPointState();
}

class _PickUpPointState extends State<PickUpPoint> {
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(55.797263945006584, 49.11133995065905),
    zoom: 17,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  // Completer<GoogleMapController> _controller = Completer();
  late GoogleMapController _controller;

  late BitmapDescriptor customIcon;
  LatLng _target =  LatLng(37.43296265331129, -122.08832357078792);

  @override
  void initState() {
    super.initState();
    print('hello');
    setCustomMarker();
  }

  void setCustomMarker() async {
    setState(() {});
    customIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(
          devicePixelRatio: 1,
        ),
        'assets/img/location3.png');
    markers.add(Marker(
      markerId: MarkerId('1'),
      position: LatLng(55.797263945006584, 49.11133995065905),
      infoWindow: InfoWindow(title: 'La Sagrada Familia'),
      icon: customIcon,
    ));
  }

  Set<Marker> markers = {};

  @override
  Widget build(BuildContext context) {
    // markers.add( Marker(
    //   markerId: MarkerId('1'),
    //   position: LatLng(55.797263945006584, 49.11133995065905),
    //   infoWindow: InfoWindow(title: 'La Sagrada Familia'),
    //   icon: BitmapDescriptor.defaultMarker,
    // ));
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Constants.borderRadius),
          border: this.widget.isOpen
              ? Border.all(color: Theme.of(context).primaryColor, width: 3)
              : Border.all(color: Color(0xFFEDE3EC))),
      padding: this.widget.isOpen ? EdgeInsets.all(15) : EdgeInsets.all(17),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Казань, ул.Мусина 29 (ТК "Стройка")',
            style: TextStyle(fontSize: 16),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            'ежедневно, 10:00 — 22:00',
            style: TextStyle(
              fontSize: 12,
              color: Theme.of(context).colorScheme.chipTextColor,
            ),
          ),
          Visibility(
            visible: this.widget.isOpen,
            child: Column(
              children: [
                SizedBox(
                  height: 14,
                ),
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Stack(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(6),
                        child: GoogleMap(
                          mapType: MapType.normal,
                          markers: markers,
                          initialCameraPosition: _kGooglePlex,
                          zoomControlsEnabled: false,
                          onMapCreated: (GoogleMapController controller) {
                            this._controller = controller;
                          },
                          gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                            new Factory<OneSequenceGestureRecognizer>(() => new EagerGestureRecognizer(),),
                          ].toSet(),
                          onCameraMove: (CameraPosition position) {
                            setState(() {
                              this._target = position.target;
                            });
                          },
                        ),
                      ),
                      Positioned(
                        top: 50 + 12.5,
                        right: 9,
                        child: ZoomButton(
                          onTapPlus: () async {
                            var currentZoomLevel =
                                await _controller.getZoomLevel();

                            currentZoomLevel = currentZoomLevel + 2;
                            _controller.animateCamera(
                              CameraUpdate.newCameraPosition(
                                CameraPosition(
                                  target: _target,

                                  zoom: currentZoomLevel,
                                ),
                              ),
                            );
                          },
                          onTapMinus: () async {
                            var currentZoomLevel =
                            await _controller.getZoomLevel();
                            currentZoomLevel = currentZoomLevel - 2;
                            _controller.animateCamera(
                              CameraUpdate.newCameraPosition(
                                CameraPosition(
                                  target: _target,
                                  zoom: currentZoomLevel,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                      // Positioned(
                      //   top: 50 + 12.5,
                      //   right: 9,
                      //   child: Card(
                      //     elevation: 2,
                      //     child: Container(
                      //       color: Color(0xFFFAFAFA),
                      //       width: 40,
                      //       height: 75,
                      //       child: Column(
                      //         children: <Widget>[
                      //           IconButton(
                      //               icon: Icon(Icons.add),
                      //               onPressed: () async {
                      //                 var currentZoomLevel = await _controller
                      //                     .getZoomLevel();
                      //
                      //                 currentZoomLevel = currentZoomLevel + 2;
                      //                 _controller.animateCamera(
                      //                   CameraUpdate.newCameraPosition(
                      //                     CameraPosition(
                      //                       target: _kGooglePlex.target,
                      //                       zoom: currentZoomLevel,
                      //                     ),
                      //                   ),
                      //                 );
                      //               }),
                      //           SizedBox(height: 2),
                      //           IconButton(
                      //               icon: Icon(Icons.remove),
                      //               onPressed: () async {
                      //                 var currentZoomLevel = await _controller
                      //                     .getZoomLevel();
                      //                 currentZoomLevel = currentZoomLevel - 2;
                      //                 if (currentZoomLevel < 0) currentZoomLevel = 0;
                      //                 _controller.animateCamera(
                      //                   CameraUpdate.newCameraPosition(
                      //                     CameraPosition(
                      //                       target: _kGooglePlex.target,
                      //                       zoom: currentZoomLevel,
                      //                     ),
                      //                   ),
                      //                 );
                      //               }),
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
