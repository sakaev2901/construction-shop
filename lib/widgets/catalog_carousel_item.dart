import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/screens/category_screen.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class CatalogCarouselItem extends StatelessWidget {
  final Widget child;
  bool isIconButton;

  CatalogCarouselItem(
      {Key? key, required this.child, this.isIconButton = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: () => Navigator.of(context).push(CupertinoPageRoute(builder: (_) => CategoryScreen())),
      child: Column(
        children: [
          Container(
            height: 54,
            width: 70,
            padding: EdgeInsets.all(5),
            decoration: this.isIconButton
                ? BoxDecoration(
                    borderRadius: BorderRadius.circular(Constants.borderRadius),
                    color: Theme.of(context).primaryColor,
                  )
                : BoxDecoration(
                    borderRadius: BorderRadius.circular(Constants.borderRadius),
                    border: Border.all(color: Theme.of(context).accentColor),
                  ),
            child: child,
          ),
          SizedBox(
            height: 7,
          ),
          Container(
            width: 70,
            child: Text(
              'Каталог оборудования',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Theme.of(context).hintColor,
                fontSize: 10,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
