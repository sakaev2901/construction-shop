import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';

class ReviewCard extends StatelessWidget {
  const ReviewCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .75,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 4,
            color: Theme.of(context).primaryColor,
          ),
          SizedBox(
            height: 4,
          ),
          Container(
            decoration: BoxDecoration(
              color: Color(0xFFF3F3F3),
            ),
            padding: EdgeInsets.only(
              left: 20,
              top: 20,
              right: 20,
              bottom: 42,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Новикова Н.А.',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text(
                          '25 марта 2021',
                          style:
                              TextStyle(fontSize: 12, color: Color(0xFFA6A6A6)),
                        )
                      ],
                    ),
                    Spacer(),
                    Container(
                      height: 36,
                      width: 36,
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(18),
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Icon(
                          CustomIcons.profile_7_1,
                          color:
                              Theme.of(context).colorScheme.selectedIconColor,
                          size: 24,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 41,
                ),
                Text(
                  'Хороший сервис проката и аренды инструмента!',
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      height: 1.6),
                ),
                Text(
                  'Беру периодически. Инструмент исправен. Вежливое обслуживание и консультации. За небольшие задержки не просят доплату. Из недостатков: не все инструменты на сайте есть в наличии',
                  style: TextStyle(
                    fontStyle: FontStyle.italic,
                    fontSize: 16,
                    height: 1.6,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
