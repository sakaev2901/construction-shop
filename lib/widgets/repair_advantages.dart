import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mobile/widgets/custom_divider.dart';

class RepairAdvantages extends StatelessWidget {

  final Map<String, String> repairAdvantages = {
    'Бесплатная диагностика': 'repair_adv1.svg',
    'Филиалы по всей России': 'repair_adv2.svg',
    'Гарантия на ремонт от 1 го месяца': 'repair_adv3.svg',
    'Доставка до объекта и забор на ремонт': 'repair_adv4.svg',
    'Запчасти по оптовым ценам': 'repair_adv5.svg',
  };

  RepairAdvantages({Key? key}) : super(key: key);

  List<Widget> _buildRepairAdvantages() {
    List<Widget> repairAdvantageWidgets = [];

    repairAdvantageWidgets.add(CustomDivider.zero());

    repairAdvantages.forEach((key, value) {
      repairAdvantageWidgets.add(
          Container(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Expanded(flex: 9,child: Text(key)),
                Spacer(),
                Expanded(flex: 1,child: SvgPicture.asset('assets/svg/$value', width: 32,)),
                SizedBox(width: 10,),
              ],
            ),
          )
      );
      repairAdvantageWidgets.add(CustomDivider.zero());
    });

    return repairAdvantageWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ..._buildRepairAdvantages()
      ],
    );
  }
}
