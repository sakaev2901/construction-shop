import 'package:flutter/material.dart';
import 'package:mobile/widgets/item/rent_time.dart';

import '../custom_divider.dart';
import 'count_picker.dart';

class ItemMainWidget extends StatelessWidget {
  final double imageWidth;

  ItemMainWidget({Key? key,required this.imageWidth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(),
                width: this.imageWidth,
                child: Image.asset(
                  'assets/img/cart_item.png',
                  fit: BoxFit.contain,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 27,
          ),
          Text(
            'Аккумуляторный перфоратор Bosch GBH 180-LI',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 22,
          ),
          Row(
            children: [
              Expanded(
                child: Column(
                  children: [
                    CustomDivider.zero(),
                    RentTime(timeLabel: 'Сутки', cost: '1 000 ₽'),
                    CustomDivider.zero(),
                    RentTime(timeLabel: '2-7 сут', cost: '900 ₽'),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  children: [
                    CustomDivider.zero(),
                    RentTime(timeLabel: '8-14 сут', cost: '800 ₽'),
                    CustomDivider.zero(),
                    RentTime(timeLabel: '>15 сут', cost: '700 ₽'),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(
            height: 18,
          ),
          Row(
            children: [
              Expanded(
                child: CountPicker(label: 'Кол-во дней', initCount: 5,),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: CountPicker(label: 'Кол-во штук', initCount: 1,),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
