import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class CountPicker extends StatefulWidget {
  final String label;
  int initCount;

  CountPicker({Key? key,required this.label,required this.initCount}) : super(key: key);

  @override
  _CountPickerState createState() => _CountPickerState();
}

class _CountPickerState extends State<CountPicker> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 9,
        bottom: 9,
        left: 10,
        right: 10,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Constants.borderRadius),
        border: Border.all(color: Theme.of(context).accentColor, width: 1),
      ),
      child: Column(
        children: [
          Text(
            this.widget.label,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: Theme.of(context).colorScheme.secondaryTextColor,
            ),
          ),
          SizedBox(
            height: 7,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 34,
                width: 34,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Theme.of(context).accentColor,
                ),
                child: CustomInkWell(
                  onTap: () {
                    setState(() {
                      if (this.widget.initCount > 1) {
                        this.widget.initCount--;
                      }
                    });
                  },
                  child: Icon(
                    CustomIcons.minus,
                    color: Theme.of(context).colorScheme.selectedIconColor,
                    size: 2,
                  ),
                ),
              ),
              Text('${this.widget.initCount}', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20,),),
              Container(
                height: 34,
                width: 34,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Theme.of(context).primaryColor,
                ),
                child: CustomInkWell(
                  onTap: () {
                    setState(() {
                      this.widget.initCount++;
                    });
                  },
                  child: Icon(
                    CustomIcons.plus,
                    color: Theme.of(context).colorScheme.selectedIconColor,
                    size: 20,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
