import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';

class RentTime extends StatelessWidget {
  final String timeLabel;
  final String cost;

  const RentTime({Key? key, required this.timeLabel, required this.cost})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 11, bottom: 11, left: 10, right: 10),
      child: Row(
        children: [
          Text(
            this.timeLabel,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondaryTextColor,
              fontWeight: FontWeight.w700,
            ),
          ),
          Spacer(),
          Text(
            this.cost,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
