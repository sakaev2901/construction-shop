import 'package:flutter/material.dart';
import 'package:mobile/config/constants.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class RelatedItem extends StatefulWidget {
  final String name;
  final String imageUrl;
  final String cost;
  bool isAdded;

  RelatedItem(
      {Key? key,
      required this.name,
      required this.cost,
      this.isAdded = false,
      required this.imageUrl})
      : super(key: key);

  @override
  _RelatedItemState createState() => _RelatedItemState();
}

class _RelatedItemState extends State<RelatedItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Image.asset(
                this.widget.imageUrl,
                fit: BoxFit.contain,
              )),
          SizedBox(
            width: 22,
          ),
          Expanded(
            flex: 10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.widget.name,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 12),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  this.widget.cost,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 6,
                ),
                Container(
                  padding: EdgeInsets.only(
                    top: 6,
                    bottom: 6,
                    left: 5,
                    right: 5,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: this.widget.isAdded
                        ? Theme.of(context).primaryColor
                        : Theme.of(context).accentColor,
                  ),
                  child: CustomInkWell(
                    onTap: () {
                      setState(() {
                        this.widget.isAdded = !this.widget.isAdded;
                      });
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 2),
                          child: Text(
                            this.widget.isAdded ? 'Добавлено' : 'Добавить к заказу',
                          ),
                        ),
                        Visibility(
                          visible: this.widget.isAdded,
                          child: Container(
                            height: 18,
                            width: 18,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  Constants.checkboxBorderRadius),
                              color:
                                  Theme.of(context).colorScheme.selectedIconColor,
                            ),
                            child: Icon(
                              CustomIcons.success,
                              size: 11,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
