import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/item/characteristic.dart';

class CharacteristicsList extends StatelessWidget {
  CharacteristicsList({Key? key}) : super(key: key);

  final Map<String, String> chars = {
    'Вес': '2 кг',
    'Мощность': '1,7 Дж',
    'Напряжение': '18 В',
    'Число оборотов': 'до 1800 об/мин',
    'Тип патрона': 'SDS-Plus',
  };

  List<Widget> _buildCharsWidgets() {
    List<Widget> charsWidgets = [];
    charsWidgets.add(CustomDivider.zero());
    chars.forEach((key, value) {
      charsWidgets.add(CharacteristicWidget(name: key, value: value));
      charsWidgets.add(CustomDivider.zero());
    });
    return charsWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Характеристики',
            style: TextStyle(
                fontSize: 20,
                color: Theme.of(context).colorScheme.chipTextColor),
          ),
          SizedBox(height: 24,),
          ..._buildCharsWidgets(),
        ],
      ),
    );
  }
}
