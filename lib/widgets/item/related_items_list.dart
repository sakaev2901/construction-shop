import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_divider.dart';
import 'package:mobile/widgets/item/related_item.dart';

class RelatedItemsList extends StatelessWidget {
  const RelatedItemsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Сопутствующие товары',
            style: TextStyle(
                fontSize: 20,
                color: Theme.of(context).colorScheme.chipTextColor),
          ),
          SizedBox(
            height: 14,
          ),
          CustomDivider.zero(),
          RelatedItem(name: 'Экспресс доставка по городу (Пикап)', cost: "150 ₽", imageUrl: 'assets/img/relate1.png'),
          CustomDivider.zero(),
          RelatedItem(name: 'Очки для защиты глаз', cost: "200 ₽", imageUrl: 'assets/img/relate2.png', isAdded: true,),
          CustomDivider.zero(),
          RelatedItem(name: 'Одноразовый респиратор', cost: "100 ₽", imageUrl: 'assets/img/relate3.png'),
          CustomDivider.zero(),
        ],
      ),
    );
  }
}
