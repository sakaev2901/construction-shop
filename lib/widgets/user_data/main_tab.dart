import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/input/custom_text_input.dart';
import 'package:mobile/widgets/main_button.dart';
import 'package:mobile/widgets/user_data/data_batch_title.dart';

class MainTab extends StatelessWidget {
  const MainTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 33,),
        DataBatchTitle(title: 'Ваши контакты'),
        SizedBox(height: 17,),
        CustomTextInput(label: 'Ваше имя', hint: 'Имя', bottomMargin: 20,),
        CustomTextInput(label: 'Фамилия', hint: 'Фамилия', bottomMargin: 20,),
        CustomTextInput(label: 'Номер телефона', hint: '+7 XXX XXX XXXX', bottomMargin: 20,),
        CustomTextInput(label: 'Электронная почта', hint: 'name@mail.com', bottomMargin: 20,),
        MainButton(
          onPressed: () => print(''),
          text: 'Сохранить',
        ),
        SizedBox(height: 46,),
        DataBatchTitle(title: 'Изменить пароль'),
        SizedBox(height: 17,),
        CustomTextInput(label: 'Ваш старый пароль', hint: '•  •  •  •  •  •  •  •  •  •  •  •  •', bottomMargin: 20,),
        CustomTextInput(label: 'Придумайте пароль', hint: '•  •  •  •  •  •  •  •  •  •  •  •  •', bottomMargin: 20,),
        CustomTextInput(label: 'Повторите новый пароль', hint: '•  •  •  •  •  •  •  •  •  •  •  •  •', bottomMargin: 20,),
        MainButton(
          enabled: false,
          onPressed: () => print(''),
          text: 'Сохранить',
        ),
        SizedBox(height: 56,),
      ],
    );
  }
}
