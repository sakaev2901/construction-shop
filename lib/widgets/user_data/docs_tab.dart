import 'package:flutter/material.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/user_data/document_widget.dart';

class DocumentsTab extends StatelessWidget {
  const DocumentsTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 33,
        ),
        DocumentWidget(
            companyName: 'Контрагент  •  ООО “Высокие технологии”',
            documentTitle: 'Договор на оказание услуг аренды #548995217',
            cost: '42 000 ₽',
            date: '28 октября 2020'),
        SizedBox(height: 20,),
        DocumentWidget(
            companyName: 'Контрагент  •  ООО “Высокие технологии”',
            documentTitle: 'Счет на оплату по договру #548995217 ',
            cost: '42 000 ₽',
            date: '28 октября 2020'),
        SizedBox(height: 20,),
        DocumentWidget(
            companyName: 'Контрагент  •  ООО “Высокие технологии”',
            documentTitle: 'Договор на оказание услуг аренды #548995217',
            cost: '42 000 ₽',
            date: '28 октября 2020'),
        SizedBox(height: 20,),
      ],
    );
  }
}
