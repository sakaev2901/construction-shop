import 'package:flutter/material.dart';
import 'package:mobile/icons/custom_icons_icons.dart';
import 'package:mobile/extensions/color_scheme_extension.dart';
import 'package:mobile/widgets/custom_ink_well.dart';

class SortBottomSheetItem extends StatelessWidget {
  bool isActive;
  final String name;
  GestureTapCallback? onTap;

  SortBottomSheetItem({this.isActive = false,required this.name, this.onTap});

  @override
  Widget build(BuildContext context) {
    return CustomInkWell(
      onTap: this.onTap,
      child: Container(
        padding: EdgeInsets.only(
          top: 21,
          bottom: 21,
          left: 8,
          right: 8,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              this.name,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  color: this.isActive ? Theme.of(context).colorScheme.primaryTextColor : Theme.of(context).colorScheme.secondaryTextColor,
              ),
            ),
            this.isActive ? Icon(
              CustomIcons.success,
              size: 16,
              color: Theme.of(context).colorScheme.selectedIconColor,
            ) : SizedBox(width: 16,)
          ],
        ),
      ),
    );
  }
}
